/**
 * Dedicated script which wraps (abstracts) all the calls to cryptoCompare api.
 */
import request from 'request-promise'
import queryString from 'query-string'

import config from '../config'

const apiRoot = config.cryptoCompareApiRoot

/**
 * Calls the crypto compare api for the current trading info which refers to
 * the traded prices.
 * @param  {Array} fromSymbol Array of crypto coins to return the data for.
 * @param  {Array} toSymbol   Array of currencies in which to send the coin
 * traded prices.
 * @return {Promise}          Promise which resolves depending on the response.
 */
export const getCurrentTradingInfo = (fromSymbol, toSymbol) => {
  const qs = queryString.stringify({
    fsyms: fromSymbol.length ? fromSymbol.join(',') : fromSymbol,
    tsyms: toSymbol.length ? toSymbol.join(',') : toSymbol
  })

  return request.get({
    uri: `${apiRoot}/pricemultifull?${qs}`,
    json: true
  })
}

/**
 * Calls the crypto compare api for the list of all supported crypto coins.
 * @return {Promise}          Promise which resolves depending on the response.
 */
export const getCoinList = () => {
  return request.get({
    uri: `${apiRoot}/all/coinlist`,
    json: true
  })
}

/**
 * NOT USED! Calls the crypto compare api for the hour historical data.
 * @return {Promise}          Promise which resolves depending on the response.
 */
export const getHourHistoricalData = (fromSymbol, toSymbol) => {
  const qs = queryString.stringify({
    fsyms: fromSymbol.length ? fromSymbol.join(',') : fromSymbol,
    tsyms: toSymbol.length ? toSymbol.join(',') : toSymbol,
    limit: 60
  })

  return request.get({
    uri: `${apiRoot}/histohour?${qs}`,
    json: true
  })
}

export default {
  getCurrentTradingInfo,
  getCoinList,
  getHourHistoricalData
}
