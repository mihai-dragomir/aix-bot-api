import BestBidOfferCalculator from
  '../best-bid-offer-calculator/BestBidOfferCalculator'
import { arrayHelper } from '../../common'
import errors from '../../common/errors'

/**
 * Higher order class to compose best spread objects using the data from
 * BestBidOfferCalculator.
 * @type {Class}
 */
class BestSpreadCalculator extends BestBidOfferCalculator {
  /**
   * Creates a class instance and assigns all local functions to its scope.
   * @return {OffersService} A class instance.
   */
  constructor () {
    super()

    this.findChainBestSpreadTransactions =
      this.findChainBestSpreadTransactions.bind(this)
    this.formatResult = this.formatResult.bind(this)
  }

  /**
   * Searches for the best bid and offer values for the given chain, forms the
   * best spread and returns it.
   * @param  {ChainEntity} chain The chain for which to generate the best spread.
   * @return {[type]}       [description]
   */
  findChainBestSpreadTransactions (chain) {
    let tsIds = chain.getTransactionsSessionIds()
    // for each transactions session id of the chain, compute the max bid transactions
    let maxBidQueryPromises = tsIds.map(
      super.findSessionTransactionsWithMaxBid
    )
    // for each transactions session id of the chain, compute the min offer transactions
    let minOfferQueryPromises = tsIds.map(
      super.findSessionTransactionsWithMinOffer
    )

    return Promise
      .all([
        Promise.all(maxBidQueryPromises),
        Promise.all(minOfferQueryPromises)
      ])
      .then(([[maxBids], [minOffers]]) => {
        let overallMaxBids = // compute the max bids of the chain
          arrayHelper.extractAllMax(maxBids, e => e.bidValue)
        let oveallMinOffers = // compute the min offers of the chain
          arrayHelper.extractAllMin(minOffers, e => e.offerValue)
        let result = {
          highestBids: overallMaxBids,
          lowestOffers: oveallMinOffers
        }

        return this.formatResult(result)
      })
  }

  /**
   * Receives the highest bid and lowest offer transactions, extracts the bid
   * and offer value which should be common in all the received transactions,
   * returns an object with the extracted: bid value, offer value,
   * bid transaction ids, offer transaction ids.
   * @param  {Array} highestBids  The highest bids array.
   * @param  {Array} lowestOffers The lowest offers array.
   * @return {Object}              The input data arranged into a more usable
   * structure.
   */
  formatResult ({ highestBids, lowestOffers }) {
    if (!highestBids || !lowestOffers) {
      throw errors.InvalidBestSpreadFormatResultInputError
    }

    let result = {}

    if (highestBids.length === 0) {
      result.highestBid = undefined
      result.highestBidTransactionIds = []
    } else {
      result.highestBid = highestBids[0].bidValue
      result.highestBidTransactionIds = highestBids.map(e => e.id)
    }

    if (lowestOffers.length === 0) {
      result.lowestOffer = undefined
      result.lowestOfferTransactionIds = []
    } else {
      result.lowestOffer = lowestOffers[0].offerValue
      result.lowestOfferTransactionIds = lowestOffers.map(e => e.id)
    }

    return result
  }
}

export default BestSpreadCalculator
