/**
 * Dedicated script which wraps (abstracts) all the calls to botmaster api.
 */
import async from 'async'

import request from 'request-promise'
import { BotMessage } from '../api/bot-message'

import { botmasterApiRoot } from '../config'

/**
 * Abstracts the request for sending a message to a telegram trader.
 * @param  {String} sessionId The trader's user session id.
 * @param  {String} messages  The message to send.
 * @param  {TraderEntity} trader    The trader document.
 * @return {Promise}           Promise which resolves or fails depending on
 * the response status.
 */
export const sendMessageToTrader = (sessionId, messages, trader) => {
  return new Promise((resolve, reject) => {
    messages = typeof messages === 'string' ? [messages] : messages

    async.eachSeries(messages, (message, callback) => {
      message = fillTraderName(message, trader.getFirstName())

      BotMessage.create({
        message: message,
        senderId: trader.getTelegramId(),
        sessionId: sessionId,
        isBotMessage: true
      })
        .catch((err) => {
          console.log('Failed to store trader message to db, ERR:', err)

          return callback(err)
        })

      request
        .post({
          method: 'POST',
          uri: `${botmasterApiRoot}/message`,
          body: {
            message,
            receiverId: trader.getTelegramId()
          },
          json: true
        })
        .then(() => callback())
        .catch(err => callback(err))
    }, err => {
      if (err) {
        return reject(err)
      }

      resolve()
    })
  })
}

/**
 * Abstracts the request for displaying buttons to a telegram trader.
 * @param  {Array} buttonLabelsArr The array containing the labels to be shown on the buttons.
 * @param  {TraderEntity} trader   The trader document.
 * @return {Promise} Promise which resolves or fails depending on the response
 * status.
 */
export const sendButtonsToTrader = (buttonLabelsArr, trader) => (
  sendMessageToTrader(
    null,
    '<buttons>' + buttonLabelsArr.join('|') + '</buttons>',
    trader
  )
)

/**
 * Replaces "{trader}" placeholders with the individual trader names.
 * @param  {String} message    The message in which to replace the placeholders.
 * @param  {String} traderName The trader name to be put in place of the
 * placeholders.
 * @return {String}            New message with placeholders replaced by
 * trader name.
 */
const fillTraderName = (message, traderName) => {
  if (typeof message !== 'string') {
    return message
  }

  return message.replace(/\{trader\}/g, traderName)
}

export default {
  sendMessageToTrader,
  sendButtonsToTrader
}
