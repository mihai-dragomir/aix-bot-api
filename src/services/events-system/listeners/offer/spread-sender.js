import Listener from '../../Listener'
import events, { eventNames } from '../../event-emitter'
import { SmartManager } from '../../../../managers'
import { TransactionsSessionRepo } from '../../../../repositories'
import messages from '../../../../user-messages'
import {
  TRANSACTIONS_SESSION_STATUSES,
  DECISION_CONTROL_HOOKS
} from '../../../../constants'

/**
 * Provides callback function to compose and send spread message to initiator.
 * @type {Class}
 */
class SpreadSenderListener extends Listener {
  /**
   * Create listener class instance.
   */
  constructor () {
    super(eventNames.SEND_SPREAD)
  }

  /*
    Receives transactionsSessionId of the active transaction, and the
    configuration of the hook that triggered this event. Sends spread to
    initiator.
   */
  listenerFn ({ transactionsSessionId, hookConfig }) {
    console.log('Executing SEND_SPREAD listener')

    SmartManager
      .findAsManyDependenciesOf({ transactionsSessionId })
      .then(({ chain, transactionsSession, financialInstrument }) => {
        let chainBestOffers = chain.getCurrentBestOffers()
        // check the hook trigger function name to determine if this is a best spread
        let isBestSpread = hookConfig.triggerFn ===
          DECISION_CONTROL_HOOKS.SHOULD_SEND_BEST_OFFER

        // builds an spreads information object
        let spreadInfo = {
          highestBid: chainBestOffers.highestBid,
          lowestOffer: chainBestOffers.lowestOffer,
          remainingQuantity: chain.getAvailableShards() *
            chain.getShardVolume(),
          initiatorTraderId: chain.getInitiatorId(),
          initiatorTraderSessionId: chain.getInitiatorTraderSession(),
          financialInstrument: financialInstrument.getLabel(),
          traderMessage: messages.initiator.spreadFn({
            isIcebergTrade: chain.isIcebergTrade(),
            isBestSpread: isBestSpread,
            smallestSpread: messages.spreadFn(
              chainBestOffers.highestBid,
              chainBestOffers.lowestOffer
            ),
            shardVolume: chain.getShardVolume(),
            financialInstrument: financialInstrument.getLabel(),
            numberOfSpreadsAlreadySent: chain.getNumberOfOffersSentToInitiator()
          })
        }

        // update chain stats
        chain.setPreviousBestOffers(chain.getCurrentBestOffers())
        chain.setNumberOfOffersSentToInitiator(
          chain.getNumberOfOffersSentToInitiator() + 1
        )
        if (!chain.getFirstOfferSentAt()) {
          chain.setFirstOfferSentAt(new Date())
        }

        // update receiver session context
        let receiverSessionInitData =
          chain.getReceiverSessionInitializationData()

        /* The watsonContext following updates are needed in order for watson to
        understand correctly the trader answer to the offer */

        // update the sender_bid to the current spread bid value
        receiverSessionInitData.watsonContext.sender_bid =
          spreadInfo.highestBid
        // update the sender_offer to the current spread offer value
        receiverSessionInitData.watsonContext.sender_offer =
          spreadInfo.lowestOffer

        chain.setReceiverSessionInitializationData(receiverSessionInitData)
        chain.save()

        TransactionsSessionRepo.updateByIds(
          chain.getTransactionsSessionIds(),
          { overallStatus: TRANSACTIONS_SESSION_STATUSES.SPREAD_CALCULATED }
        )

        // send message
        events.emit(eventNames.SEND_SPREAD_MESSAGE, spreadInfo)
      })
  }
}

export default new SpreadSenderListener()
