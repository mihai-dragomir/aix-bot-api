import Listener from '../../Listener'
import events, { eventNames } from '../../event-emitter'
import botmasterAPI from '../../../botmaster-api-wrapper'
import dashboardAPI from '../../../dashboard-api-wrapper'
import { Trader } from '../../../../api/trader'
import { TRADE_ACTION_BUTTON_LABELS } from '../../../../constants'

/**
 * Provides callback function to send the given spread message to initiator.
 * @type {Class}
 */
class SpreadMessageSenderListener extends Listener {
  /**
   * Create listener class instance.
   */
  constructor () {
    super(eventNames.SEND_SPREAD_MESSAGE)
  }

  /**
    Uses botmaster api wrapper to send the traderMessage to the initiator
    trader.
    Overwrites the initiator trader watson context to properly understand its
    answer for the offer.
  */
  listenerFn ({
    highestBid,
    lowestOffer,
    remainingQuantity,
    traderMessage,
    initiatorTraderId,
    initiatorTraderSessionId
  }) {
  // find the trader document in db
    Trader
      .findById(initiatorTraderId)
      .then(trader => {
      // call botmaster send message fuction
        botmasterAPI
          .sendMessageToTrader(initiatorTraderSessionId, traderMessage, trader)
          .then(resp => {
            console.log('Smallest spread message was sent to the initiator.')

            return botmasterAPI.sendButtonsToTrader(
              TRADE_ACTION_BUTTON_LABELS,
              trader
            )
          })
          .then(resp => {
            console.log('Trade option buttons were sent to the initiator.')
          })
          .catch(err => {
            console.log('MESSAGE FAILED to send to initiator, ERR:', err)
          })

        /* Emit an event to overwrite the spread related watson context fields of
        the initiator. */
        events.emit(eventNames.OVERWRITE_TRADERS_SESSION, {
          traderIds: [ initiatorTraderId ],
          watsonContext: {
            market_bid: highestBid,
            market_offer: lowestOffer,
            market_remaining_quantity: remainingQuantity
          },
          setContext: true
        })

        dashboardAPI.notifyDashboardTransactions()
      })
  }
}

export default new SpreadMessageSenderListener()
