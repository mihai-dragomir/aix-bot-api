import difference from 'lodash/difference'
import uniq from 'lodash/uniq'

import Listener from '../Listener'
import events, { eventNames } from '../event-emitter'
import {
  TransactionRepo,
  TransactionsSessionRepo,
  TransactionsSessionChainRepo
} from '../../../repositories'
import {
  TransactionsSessionManager,
  TransactionsSessionChainManager
} from '../../../managers'
import messages from '../../../user-messages'
import {
  TRANSACTIONS_SESSION_STATUSES,
  SESSIONS_CHAIN_STATUSES,
  TRANSACTION_STATUSES
} from '../../../constants'

/**
 * Responsible for doing the necessary changes to finalize an iceberg trade.
 * @type {Class}
 */
class IcebergTradeFinalizerListener extends Listener {
  constructor () {
    super(eventNames.FINALIZE_ICEBERG_TRADE)
  }

  listenerFn ({ transactionsSessionId, chainId }) {
    console.log('Executing FINALIZE_ICEBERG_TRADE listener')
    let chain = null
    let financialInstrument = null
    let acceptedTransactions = []

    // find the decessary dependencies to work with
    TransactionsSessionManager
      .findAllDependenciesOfTransactionsSessionWithId(transactionsSessionId)
      .then(dependencies => {
        return Promise.all([
          TransactionRepo.findAcceptedTransactionsOfChainWithId(chainId),
          Promise.resolve(dependencies)
        ])
      })
      .then(([transactions, dependencies]) => {
        acceptedTransactions = transactions
        chain = dependencies.chain
        financialInstrument = dependencies.financialInstrument

        // stop all periodic check loops
        TransactionsSessionChainManager.stopAllPeriodicCheckLoops(chain)

        // update sessions statuses
        // first set all to closed
        return TransactionsSessionRepo
          .updateByIds(
            chain.getTransactionsSessionIds(),
            { overallStatus: TRANSACTIONS_SESSION_STATUSES.CLOSED }
          )
          .then(() => {
            let acceptedTransactionsSessionIds = acceptedTransactions
              .map(t => t.getTransactionsSessionId())

            // then set closed_by_confirm to the ones with accepted transactions
            return TransactionsSessionRepo
              .updateByIds(
                uniq(acceptedTransactionsSessionIds),
                { overallStatus: TRANSACTIONS_SESSION_STATUSES.CLOSED_BY_CONFIRM }
              )
          })
          .then(() =>
            // find the active transactions and set their status to CLOSED
            TransactionsSessionChainRepo
              .findChainReceiverTransactionIds(chain.getId(), {
                fromNotClosedAndNotDecidedTransactions: true
              })
              .then(transactionIds => {
                TransactionRepo.updateByIds(transactionIds, {
                  status: TRANSACTION_STATUSES.CLOSED
                })
              })
          )
          .then(() => {
            return TransactionsSessionChainRepo
              .findChainReceiverTraderIds(chain.getId())
          })
      })
      // send messages to users
      .then(allReceiverIds => {
        let acceptedTransactionReceiverIds = acceptedTransactions
          .map(t => t.getReceiverId())
        let initiatorIds = [chain.getInitiatorId()]

        let otherReceiverIds = difference(
          allReceiverIds,
          acceptedTransactionReceiverIds
        )

        let remainingShards = chain.getAvailableShards()
        let remainingVolume = remainingShards
          ? chain.getShardVolume() * remainingShards : null
        let soldVolume = remainingShards
          ? chain.getTotalTradedVolume() - remainingVolume : null

        // prepare initiator message
        let vars = {
          remainingVolume: remainingVolume,
          soldVolume: soldVolume,
          totalVolume: chain.getTotalTradedVolume(),
          price: chain.getPriceToleranceLimit(),
          financialInstrument: financialInstrument.getLabel()
        }

        // inform initiator
        events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
          traderIds: initiatorIds,
          message: messages.initiator.icebergTransactionCongratulationFn(vars)
        })
        // inform receivers
        events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
          traderIds: otherReceiverIds,
          message: messages.receiver.thankForParticipationFn()
        })

        chain.setStatus(SESSIONS_CHAIN_STATUSES.FINISHED)

        return chain.save()
      })
  }
}

export default new IcebergTradeFinalizerListener()
