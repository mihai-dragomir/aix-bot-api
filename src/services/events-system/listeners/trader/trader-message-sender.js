import Listener from '../../Listener'
import { eventNames } from '../../event-emitter'
import botmasterAPI from '../../../botmaster-api-wrapper'
import { TraderRepo } from '../../../../repositories'

class TraderMessageSender extends Listener {
  /*
    Creates a class instance
   */
  constructor () {
    // registers the name of the event to listen for
    super(eventNames.SEND_MESSAGE_TO_TRADERS)
  }

  // The callback function to execute when the event is captured
  listenerFn ({
    sessionId,
    traderIds = [],
    message
  }) {
    console.log('Executing SEND_MESSAGE_TO_TRADERS listener', traderIds, message)

    // gets some traders data for the ones for which we have to send the message
    TraderRepo
      .findByIds(traderIds, { firstName: 1, lastName: 1, telegramId: 1 })
      .then(traders => {
        // send message for each trader using botmaster api
        traders.map(trader => {
          botmasterAPI
            .sendMessageToTrader(sessionId, message, trader)
            .then(() => {
              console.log(
                `Message successfully sent to trader ${trader.getTelegramId()}`
              )
            })
            .catch(err => {
              console.log(
                `Failed to send message to trader ${trader.getTelegramId()} ERR:`,
                err
              )
            })
        })
      })
  }
}

export default TraderMessageSender
