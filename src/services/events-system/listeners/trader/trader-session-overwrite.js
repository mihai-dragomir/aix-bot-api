import async from 'async'
import difference from 'lodash/difference'
import Listener from '../../Listener'
import { eventNames } from '../../event-emitter'
import { objectHelper } from '../../../common'
import {
  SessionRepo,
  SessionInitializationDataRepo
} from '../../../../repositories'
import {
  DELETE_SESSIONS_INSTEAD_OF_UPDATE,
  SEND_RATE_REQUEST_ONLY_TO_USERS_WITH_SESSION
} from '../../../../constants'

/**
 * Listener responsible for overwriting specific user session fields for the
 * referenced traders.
 * @type {Class}
 */
class TraderSessionOverwriteListener extends Listener {
  /**
   * Creates a class instance.
   */
  constructor () {
    // register the name of the event to listen on
    super(eventNames.OVERWRITE_TRADERS_SESSION)

    // bind the class functions to the class scope
    this.listenerFn = this.listenerFn.bind(this)
    this.overwriteUsersSession = this.overwriteUsersSession.bind(this)
    this.generateSessionUpdatesObject =
      this.generateSessionUpdatesObject.bind(this)
  }

  /*
    The callback function to execute when the event is captured. Expects an
    object parameter which should mandatory contain traderIds array, to specify
    the traders for which to overwrite session.
  */
  listenerFn (params) {
    let {
      traderIds, // the trader ids for which to overwrite the session
      ...optionalParams
    } = params

    console.log('Executing OVERWRITE_TRADERS_SESSION listener', traderIds)

    // the object which will contain all changes to the session
    let sessionUpdates = this.generateSessionUpdatesObject(optionalParams)
    let flattenedUpdates = objectHelper.flatten({ session: sessionUpdates })
    let isEmptyContext = false

    /* if watsonContext parameter has value 'empty' then assure that the entire
      watson context will be cleared */
    if (optionalParams.watsonContext === 'empty') {
      isEmptyContext = true
      flattenedUpdates['session.watson.context'] = {}
    }

    /*
      Call local function to update the trader sessions with the
      flattenedUpdates object. When the updates are executed, it calls a cb
      function with the traders which didn't have a session in db
      (this is possible sessions expire after a certain time).
     */
    setTimeout(() => {
      this.overwriteUsersSession(
        traderIds,
        flattenedUpdates,
        isEmptyContext,
        tradersWithoutSessionIds => {
          if (SEND_RATE_REQUEST_ONLY_TO_USERS_WITH_SESSION) {
            /* Do nothing more if the intention is to update only the recent
             active traders. */
            return
          }

          /* For each trader without session, create a session initialization
            data document to be used when user has the session. */
          async.eachLimit(tradersWithoutSessionIds, 20, (traderId, callback) => {
            SessionInitializationDataRepo
              .updateForTraderWithId(traderId, sessionUpdates) // pay attention NOT to save the flattened data object!
              .then(() => callback())
              .catch(() => callback())
          })
        })
    }, 500) // wait a while for other related async db operations to complete
  }

  /**
   * Checks the params object fields and adds them to an updates object.
   * @param  {Object} params Object with fields and values which should be
   * updated.
   * @return {Object} An object containing the updates necessary to be
   * done on the trader session.
   */
  generateSessionUpdatesObject (params) {
    let {
      watsonContext, // (optional) can be 'empty' or an object with the watson context fields to overwrite
      ...otherParams // fields to be put directly on session
    } = params
    let updatesObject = { ...otherParams }

    if (typeof watsonContext === 'object') {
      updatesObject.watson = updatesObject.watson || {
        context: watsonContext
      }
    }

    return updatesObject
  }

  overwriteUsersSession (traderIds, flattenedData, isEmptyContext, callback) {
    async.mapLimit(traderIds, 20, (traderId, callback) => {
      /* If watsonContext='empty' and the delete session flag is ON, deletes all
      the related session data from db instead of overwriting it, for each
      trader. */
      if (isEmptyContext && DELETE_SESSIONS_INSTEAD_OF_UPDATE) {
        return Promise
          .all([
            SessionRepo.deleteAllForTraderWithId(traderId),
            SessionRepo.deleteAllWithNoSessionData(),
            SessionInitializationDataRepo.deleteForTradeWithId(traderId)
          ])
          .then(results => {
            console.log('!! Remove results:')
            console.log('delete sessions by trader id:', results[0].result)
            console.log('delete empty sesions:', results[1].result)
            console.log('delete init data by telegram id:', results[2].result)

            callback(null, traderId)
          })
          .catch(err => {
            console.log(
              'Failed to delete session and data for trader with id:',
              traderId,
              ', ERR:',
              err
            )

            callback(null, traderId)
          })
      }

      // update the session
      return SessionRepo
        .updateAllForTraderWithId(traderId, flattenedData, callback)
    }, (errs, results) => {
      // results contains the affected trader ids
      results = results.filter(result => !!result)

      /* The difference between traderIds and results is the ids of the traders
      which do not have a session (the session could not be updated) */
      callback(difference(traderIds, results))
    })
  }
}

export default TraderSessionOverwriteListener
