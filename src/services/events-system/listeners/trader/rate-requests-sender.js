import Listener from '../../Listener'
import events, { eventNames } from '../../event-emitter'
import { FinancialInstrumentRepo } from '../../../../repositories'
import messages from '../../../../user-messages'
import dashboardAPI from '../../../dashboard-api-wrapper'
import marketPrices from '../../../market-prices'

/**
 * Listener responsible for sending the rate request message (the message that
 * requests a bid/offer price from the receiver, for a given amount of financial
 * instrument(eg. BTC)).
 * @type {Class}
 */
class RateRequestsSenderListener extends Listener {
  constructor () {
    super(eventNames.SEND_RATE_REQUEST_MESSAGES)

    this.getCurrentMarketPrice = this.getCurrentMarketPrice.bind(this)
    this.listenerFn = this.listenerFn.bind(this)
  }

  listenerFn ({
    marketMakerIds,
    financialInstrumentId,
    transactionsSession,
    chain
  }) {
    console.log('Executing SEND_RATE_REQUEST_MESSAGES listener')

    // firstly, it gets the financial instrument label (identifier)
    FinancialInstrumentRepo.findById(financialInstrumentId, { label: 1 })
      .then(financialInstrument => {
        // uses a predefined currency for now
        const currency = 'USD'

        // generate the rate request message
        const rateRequestMessage = messages.receiver.marketMakerRateRequestFn(
          chain,
          transactionsSession,
          financialInstrument.getLabel(),
          this.getCurrentMarketPrice(financialInstrument.getLabel(), currency)
        )
        /* Send a signal to the dashboard to notify that something changed
        related to transactions */
        dashboardAPI.notifyDashboardTransactions()

        events.emit(eventNames.SEND_MESSAGE_TO_TRADERS, {
          traderIds: marketMakerIds,
          message: rateRequestMessage
        })
      })
  }

  /**
   * Returns the current market price to be shown in the rate request.
   * @param  {String} financialInstrumentLabel Identifier of the financial
   * instrument.
   * @param  {String} currency The currency in which to return the price.
   * @return {Number} The current market price for the given financial
   * instrument, in the requested currency.
   */
  getCurrentMarketPrice (financialInstrumentLabel, currency) {
    return marketPrices.getCurrentPrice(financialInstrumentLabel, currency)
  }
}

export default RateRequestsSenderListener
