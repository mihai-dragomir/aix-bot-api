/** This script publishes a function which send a message to the watson API and
returns a promise which will resolve with the response data/error. */
var aixSherlock = require('aix-sherlock')

const sendMessageToWatson = params => {
  const messageForWatson = {
    context: params.context,
    workspace_id: params.workspaceId,
    input: {
      text: params.text
    }
  }

  aixSherlock.processOutgoing(messageForWatson)

  return new Promise((resolve, reject) => {
    params.watson.message(messageForWatson, (err, watsonUpdate) => {
      if (err) {
        return reject(err)
      }
      return resolve(watsonUpdate)
    })
  })
}

export default sendMessageToWatson
