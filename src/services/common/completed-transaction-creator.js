/**
 * Script specialised on adding completed transactions into database.
 */

import { CompletedTransaction } from '../../api/completed-transaction'
import { TRADE_ACTIONS } from '../../constants'

/**
 * Inserts the completed transaction document into db, based on the information
 * acquired from the confirmedTransaction document, the transactionsSession
 * document and the chain document.
 * @param  {TransactionEntity} confirmedTransaction The confirmed transaction
 * document for which the completed transaction is created.
 * @param  {TransactionsSessionEntity} transactionsSession The transactions
 * session document for which the completed transaction is created.
 * @param  {ChainEntity} chain The chain document for which the completed
 * transaction is created.
 * @return {Promise} Promise which resolves to te CompletedTransaction document.
 */
export const createCompletedTransaction = (
  confirmedTransaction,
  transactionsSession,
  chain
) => {
  return CompletedTransaction
    .create({
      tradedFinancialInstrumentId: chain.getFinancialInstrumentId(),
      initiatorTraderId: confirmedTransaction.getInitiatorId(),
      marketMakerTraderId: confirmedTransaction.getReceiverId(),
      transactionsSessionId: transactionsSession.getId(),
      transactionId: confirmedTransaction.getId(),
      dealVolume: confirmedTransaction.getOfferVolume(),
      dealMonth: transactionsSession.getRequestMonth(),
      dealYear: transactionsSession.getRequestYear(),
      dealAction: transactionsSession.getRequestAction(),
      dealValue: transactionsSession.getRequestValue(),
      dealPrice: determineDealPrice(
        confirmedTransaction,
        chain
      ),
      closeAction: confirmedTransaction.getCloseAction()
    })
}

/**
 * Picks the correct value for the transaction closing price.
 * @param  {TransactionEntity} transaction The confirmed transaction document.
 * @param  {ChainEntity} chain The chain of the confirmed transaction.
 * @return {Number} The correct transaction closing price.
 */
const determineDealPrice = (transaction, chain) => {
  let dealPrice

  switch (transaction.getCloseAction()) {
    case TRADE_ACTIONS.BUY: dealPrice = transaction.getOfferValue(); break
    case TRADE_ACTIONS.SELL: dealPrice = transaction.getBidValue(); break
    case TRADE_ACTIONS.BID: dealPrice = chain.getAskedBid(); break
    case TRADE_ACTIONS.OFFER: dealPrice = chain.getAskedOffer(); break
  }

  return dealPrice
}

export default {
  createCompletedTransaction
}
