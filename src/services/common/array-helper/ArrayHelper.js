import _ from 'lodash'

import errors from '../errors'

/**
 * Helper class to handle work with arrays.
 * @type {Class}
 */
class ArrayHelper {
  /**
   * Takes an array of elements and an iteratee function, finds the max value of
   * the input array by calling iteratee function on each element and comparing
   * the results, then returns all input elements which have the max value.
   * @param  {Array} array      The array containing the elements from which to
   * extract all max.
   * @param  {Function} iterateeFn Function to be called on each element in the
   * array, to return the value which should be sorted.
   * @return {Array}            Array with input elements having the max value.
   */
  extractAllMax (array, iterateeFn) {
    if (typeof iterateeFn !== 'function') {
      throw errors.IterateeNotAFunctionError
    }
    if (!Array.isArray(array) || array.length === 0) {
      return []
    }

    let max = _.maxBy(array, iterateeFn)
    let maxVal = iterateeFn(max)
    let filteredArr = array.filter(e => iterateeFn(e) === maxVal)

    return filteredArr
  }

  /**
   * Takes an array of elements and an iteratee function, find the min value of
   * the input array by calling iteratee function on each element and comparing
   * the results, then returns all input elements which have the min value.
   * @param  {Array} array      The array containing the elements from which to
   * extract all min.
   * @param  {Function} iterateeFn Function to be called on each element in the
   * array, to return the value which should be sorted.
   * @return {Array}            Array with input elements having the min value.
   */
  extractAllMin (array, iterateeFn) {
    if (typeof iterateeFn !== 'function') {
      throw errors.IterateeNotAFunctionError
    }
    if (!Array.isArray(array) || array.length === 0) {
      return []
    }

    let min = _.minBy(array, iterateeFn)
    let minVal = iterateeFn(min)
    let filteredArr = array.filter(e => iterateeFn(e) === minVal)

    return filteredArr
  }

  /**
   * Takes an array of indexes and an array of elements. Returns another array
   * having the input array's elements, but without the ones with the indexes
   * indicated by the indexes array.
   * @param  {[type]} indexes Array with the element indexes to be removed.
   * @param  {[type]} arr     Array with all the input elements.
   * @return {[type]}         Array with the remaining elements after removing
   * the specified indexes from the input array.
   */
  removeIndexesFromArray (indexes, arr) {
    if (!Array.isArray(arr) || !indexes || !indexes.length) {
      return []
    }

    return arr.filter((e, index) => !indexes.includes(index))
  }
}

export default ArrayHelper
