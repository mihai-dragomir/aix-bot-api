import { Trader } from '../../../api/trader'
import { Transaction } from '../../../api/transaction'
import { Session } from '../../../api/session'
import { TransactionsSession } from '../../../api/transactions-session'
import { FinancialInstrument } from '../../../api/financial-instrument'
import { SessionInitializationData } from '../../../api/session-initialization-data'

import {
  CLOSED_TRANSACTIONS_SESSION_STATUSES,
  ACTIVE_TRANSACTION_STATUSES
} from '../../../constants'

/**
 * Middleware for loading trader related data on the user session, if not
 * already loaded. Loads: trader (by telegramId), financial instruments,
 * active transaction if trader is implicated in one.
 *
 * This adds:
 * session.trader
 * session.financialInstruments
 * session.activeTransaction
 *
 * To be mounted on POST /bot-messages endpoint.
 *
 * @param {options} Middleware predefined options (Nothing at the moment).
 * @return {Function} The middleware function which should be mounted.
 */
export const traderDataMiddleware = options => {
  console.log('Trader Data Middleware')

  const loadTraderDataIfNecessary = (req, res, next) => {
    const { session, body } = req

    // don't do this in unit test mode
    if (process.env.NODE_ENV === 'unit_test') {
      return next()
    }

    initializeSessionIfCase(session, body)
      .then(() => (
        Promise
          .all([
            loadTrader(session, body.senderId),
            loadFinancialInstruments(session)
          ])
          .then(([trader, financialInstruments]) => {
            session.trader = trader
            session.financialInstruments = financialInstruments

            loadActiveTransaction(session)
              .then(transaction => {
                session.activeTransaction = transaction
                next()
              })
              .catch(err => {
                console.log('Failed to load active transaction, ERR:', err)
                next()
              })
          })
          .catch(err => {
            console.log('FSM Middleware failed to acquire initiallization date, ERR', err)
            next()
          })
      ))
      .catch(err => console.log('Error in initializeSessionIfCase, ERR:', err))
  }

  const loadTrader = (session, telegramId) => {
    if (!session.trader) {
      return Trader.findOne({ telegramId })
    }

    return Promise.resolve(session.trader)
  }

  const loadFinancialInstruments = session => {
    if (!session.financialInstruments) {
      return FinancialInstrument.find({})
    }

    return Promise.resolve(session.financialInstruments)
  }

  const loadActiveTransaction = session => {
    if (!session.trader || !session.financialInstruments) {
      return Promise.resolve(null)
    }
    // find the first active transaction where trader is mentioned as receiver
    const transactionsQueryOptions = {
      marketMakerId: session.trader._id,
      status: { $in: ACTIVE_TRANSACTION_STATUSES }
    }

    return Transaction
      .find(transactionsQueryOptions)
      .sort({ createdAt: -1 })
      .limit(1)
      .then(transactions => {
        if (!transactions[0]) {
          return Promise.resolve(null)
        }

        return TransactionsSession
          .findById(transactions[0].transactionsSessionId)
          .then(transactionsSession => {
            if (
              !transactionsSession ||
              CLOSED_TRANSACTIONS_SESSION_STATUSES.includes(
                transactionsSession.overallStatus
              )
            ) {
              return Promise.resolve(null)
            }

            return Promise.resolve(transactions[0])
          })
      })
  }

  const isNewCreatedSession = session => {
    let keys = Object.keys(session)

    return keys.length === 1 && keys[0] === 'cookie'
  }

  const traderHasExistingSession = telegramId => (
    Session
      .findOne({ 'session.trader.telegramId': telegramId })
      .then(trader => Promise.resolve(!!trader))
  )

  const initializeSessionIfCase = (session, body) => (
    new Promise((resolve, reject) => {
      if (!isNewCreatedSession(session)) {
        return resolve()
      }

      return traderHasExistingSession('' + body.senderId)
        .then(hasExistingSession => {
          if (hasExistingSession) {
            return resolve()
          }

          return SessionInitializationData
            .findOne({ traderTelegramId: '' + body.senderId })
            .then(entity => {
              if (entity) {
                if (entity.initiallizationData) {
                  Object.keys(entity.initiallizationData).forEach(key => {
                    if (key !== 'cookies') {
                      session[key] = entity.initiallizationData[key]
                    }
                  })
                }

                return entity.remove()
                  .then(() => resolve())
                  .catch(err => {
                    console.log('Failed to remove session initialization data, ERR:', err)
                    resolve()
                  })
              }

              return resolve()
            })
        })
        .catch(err => {
          console.log('New session initiallization failed, ERR:', err)

          return resolve()
        })
    })
  )

  return loadTraderDataIfNecessary
}

export default new traderDataMiddleware()
