import { watsonConfig } from '../../../config'
import _sendMessageToWatson from '../../common/send-message-to-watson'
import { USER_TYPES } from '../../../constants'
import fsmHelper from '../fsm/fsm-helper'

const watsonDeveloperCloud = require('watson-developer-cloud')
const watsonOptions = {
  settings: {
    username: watsonConfig.username,
    password: watsonConfig.password,
    url: watsonConfig.url,
    version: 'v1', // as of this writing (01 Apr 2017), only v1 is available
    version_date: '2017-05-26' // latest version-date as of this writing
  },
  workspaceId: null // Is set when message is sent based on trader type
}

/**
 * Middleware responsible for detecting which Watson workspace to use. At the
 * time of writing there is a "Crypto Watson" which understands messages for
 * crypto trade, and a "Iron Ore Watson" which understands messages for iron ore
 * trade. This middleware picks between them, by sending the user message to
 * both of them, by comparing the intent confindence returned by each response.
 *
 * To be mounted on POST /bot-messages endpoint.
 *
 * @param {options} Middleware predefined options (Nothing at the moment).
 * @return {Function} The middleware function which should be mounted.
 */
export const watsonWorkspaceDetectMiddleware = options => {
  console.log('Watson Workspace Detect Middleware')

  const commonWatsonParams = {
    watson: watsonDeveloperCloud.conversation(watsonOptions.settings)
  }

  const watsonWorkspaceDetectFn = (req, res, next) => {
    const { session, body } = req

    // don't do this in unit test mode
    if (process.env.NODE_ENV === 'unit_test') {
      return next()
    }

    session.isWatsonError = false

    /* It there is a request_crypto_symbol on the session, then it means the
    workspace was already detected and there is no need to do this now. */
    if (fsmHelper.getFinancialInstrumentLabel(session)) {
      console.log('HAS WORKSPACES DETECTED:', {
        initiatorWorkspaceId: session.initiatorWorkspaceId,
        receiverWorkspaceId: session.receiverWorkspaceId
      })

      return next()
    }

    let userMessage = body.message
    // prepare cypto watson params
    let cryptoWatsonParams = Object.assign({}, commonWatsonParams, {
      workspaceId: selectCorrectWorkspaceFromPair(session, watsonConfig.crypto),
      context: {},
      text: userMessage
    })
    // prepare iron ore watson params
    let futureCommodityWatsonParams = Object.assign({}, commonWatsonParams, {
      workspaceId: selectCorrectWorkspaceFromPair(
        session,
        watsonConfig.futureCommodity
      ),
      context: {},
      text: userMessage
    })

    Promise.all([
      _sendMessageToWatson(cryptoWatsonParams),
      _sendMessageToWatson(futureCommodityWatsonParams)
    ]).then(watsonAnswers => {
      let cryptoRateIntent = getRateRequestIntentInfo(watsonAnswers[0])
      let futureCommodityRateIntent = getRateRequestIntentInfo(watsonAnswers[1])

      if (!futureCommodityRateIntent) {
        console.log('USING CRYPTO WATSON')
        setWatsonWorkspace(session, watsonConfig.crypto)
      } else if (!cryptoRateIntent) {
        console.log('USING FUTURE_COMMODITY WATSON')
        setWatsonWorkspace(session, watsonConfig.futureCommodity)
      } else if (futureCommodityRateIntent.confidence < cryptoRateIntent.confidence) {
        console.log('USING CRYPTO WATSON')
        setWatsonWorkspace(session, watsonConfig.crypto)
      } else {
        console.log('USING FUTURE_COMMODITY WATSON')
        setWatsonWorkspace(session, watsonConfig.futureCommodity)
      }

      return next()
    })
      .catch(err => {
        console.log('Failed to call watson, ERR:', err)
        session.isWatsonError = true

        return next()
      })
  }

  const selectCorrectWorkspaceFromPair = (session, workspacesPair) => {
    if (session.userType === USER_TYPES.MARKET_MAKER) {
      return workspacesPair.receiverWorkspaceId
    }
    return workspacesPair.initiatorWorkspaceId
  }

  const getRateRequestIntentInfo = watsonAnswer => {
    let intents = watsonAnswer.intents

    for (let i = 0; i < intents.length; i++) {
      if (isRawRateRequestIntent(intents[i])) {
        return intents[i]
      }
    }

    return null
  }

  const isRawRateRequestIntent = intent => {
    return intent.intent === '1_0_rate_request'
  }

  const setWatsonWorkspace = (session, workspace) => {
    session.initiatorWorkspaceId = workspace.initiatorWorkspaceId
    session.receiverWorkspaceId = workspace.receiverWorkspaceId
  }

  return watsonWorkspaceDetectFn
}

export default new watsonWorkspaceDetectMiddleware()
