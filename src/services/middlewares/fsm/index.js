import StateMachine from 'javascript-state-machine'
import createStateMethods from './methods'
import helper from './fsm-helper'
import { env } from '../../../config'
import { sessionWrapper } from './session-wrapper'
import {
  USER_TYPES,
  TRADER_TYPES,
  STANDARD_TRANSACTIONS_SESION_CHAIN,
  MARKET_MAKERS_TRANSACTIONS_SESION_CHAIN,
  WATSON_FLOW_CONTROL
} from '../../../constants'

/**
 * Middleware responsible for handling the user messages sent by the chat
 * application. It implements a FSM = Finite State Machine, with states and
 * transitions. The middleware triggers transitions depending on the watson
 * context response variables and intent.
 *
 * For a better understanding on how the javascript-state-machine package works,
 * check: https://github.com/jakesgordon/javascript-state-machine
 *
 * To be mounted on POST /bot-messages endpoint.
 *
 * @param {options} Middleware predefined options (Nothing at the moment).
 * @return {Function} The middleware function which should be mounted.
 */
const FSMMiddlware = (options) => {
  console.log('FSM Middleware')

  /*
    Here is the list of all transitions supported by the FSM. Structure:
    name: transition name (will be used camel cased to call the transition)
    from: current state name
    to:   next state name
   */
  const transitions = [
    { name: 'announce-trader-not-registered', from: 'none', to: 'trader-not-registered-announced' },
    { name: 'announce-financial-instrument-not-active', from: 'none', to: 'financial-instrument-not-active-announced' },
    { name: 'find-new-trade-market-makers', from: 'none', to: 'new-trade-market-makers-found' },
    { name: 'send-rate-request', from: ['none', 'iceberg-trade-decision'], to: 'rate-request-sent' },
    { name: 'accept-offer', from: 'none', to: 'offer-accepted' },
    { name: 'decline-offer', from: 'none', to: 'offer-declined' },
    { name: 'decline-offer', from: 'two-way-quote-sent', to: 'offer-declined' },
    { name: 'send-two-way-quote', from: 'none', to: 'two-way-quote-sent' },
    { name: 'send-trade-request', from: ['none', 'trade-option-specified'], to: 'trade-request-sent' },
    { name: 'handle-objection', from: 'none', to: 'objection-handled' },
    { name: 'refuse-rate-request', from: 'none', to: 'two-way-quote-refused' },
    { name: 'cancel-trade', from: 'none', to: 'trade-cancelled' },
    { name: 'decide-if-iceberg-trade', from: 'none', to: 'iceberg-trade-decision' },
    { name: 'mark-trade-option-specified', from: 'none', to: 'trade-option-specified' },
    { name: 'no-op', from: '*', to: 'no-op' },
    { name: 'exit-middleware', from: '*', to: 'middleware-exited' }
  ]

  function fsmMiddlewareFn ({ session, body }, res, next) {
    // skip this middleware if in unit test mode
    if (process.env.NODE_ENV === 'unit_test') {
      return next()
    }

    // Only inform the user there is an error
    if (session.isWatsonError) {
      if (env === 'local' || env === 'development') {
        session.outputMessage = session.watsonErrorMessage
      } else {
        session.outputMessage = [
          'Sorry. It looks like our AI service has a problem. ' +
          'Please try again later.'
        ]
      }

      return next()
    }

    // define the FMS hook function params
    const stateMethodParams = {
      session: sessionWrapper.wrap(session),
      sessionWrapper: sessionWrapper,
      body: body,
      res: res,
      next: next
    }

    const fsm = new StateMachine({
      transitions,
      methods: Object.assign(
        createStateMethods(stateMethodParams), // add the additional stateMethodParams to each method
        {
          onBeforeTransition: ({transition, from, to}) => { // add additional before transition hook for debug
            console.log(`FSM TRANSITION: ${transition} (${from} -> ${to})`)
          }
        }
      )
    })

    // execute transitions
    executeFsmTransitions(fsm, session)
  };

  return fsmMiddlewareFn
}

const executeFsmTransitions = (fsm, session) => {
  console.log('watson', session.watson)
  // send the message received from watson to the user chat app
  session.outputMessage = session.watson.output.text

  // if by some reason the userType is not set on the session, use TRADER
  session.userType = session.userType || USER_TYPES.TRADER

  // if trader is not registered inform him and exit
  if (helper.isTraderNotRegistered(session)) {
    return fsm
      .announceTraderNotRegistered()
      .then(() => fsm.exitMiddleware())
      .catch(() => fsm.exitMiddleware())
  }

  let financialInstrumentLabel = helper.getFinancialInstrumentLabel(session)
  let hasFinancialInstrument = helper.checkIfTraderHasFinancianInstrument(session, financialInstrumentLabel)

  if (!!financialInstrumentLabel && !hasFinancialInstrument) {
    return fsm.announceFinancialInstrumentNotActive()
      .then(() => fsm.exitMiddleware())
  }

  // handle flow control point if specified/present on watson context
  let flowControlHandlerPromise = Promise.resolve()

  if (
    session.userType === USER_TYPES.TRADER &&
    helper.isFlowControlPoint(session) &&
    helper.getFlowControlValue(session) === WATSON_FLOW_CONTROL.ICEBERG_DECISION
  ) {
    console.log('HANDLING FLOW CONTROL POINT')
    flowControlHandlerPromise = fsm.decideIfIcebergTrade()
  }

  flowControlHandlerPromise.then(() => {
    // re-assign output messages to sync with the flow control handler changes
    session.outputMessage = session.watson.output.text

    if (helper.isRateRequestIntention(session)) {
      session.financialInstrument =
        helper.getFinancialInstrument(session, financialInstrumentLabel)
      session.userType = USER_TYPES.TRADER

      // choose weather to send rate request to preferential or standard traders
      let rateRequestStartPromise =
        session.trader.traderType === TRADER_TYPES.PREFERENTIAL
          ? fsm.sendRateRequest(MARKET_MAKERS_TRANSACTIONS_SESION_CHAIN)
          : fsm.sendRateRequest(STANDARD_TRANSACTIONS_SESION_CHAIN)

      return rateRequestStartPromise
        .then(() => fsm.exitMiddleware())
        .catch(() => {
          session.financialInstrument = null
          session.userType = null

          fsm.exitMiddleware()
        })
    }

    // receiver specific transitions
    if (session.userType === USER_TYPES.MARKET_MAKER) {
      if (helper.isTwoWayQuoteIntention(session)) {
        return fsm.sendTwoWayQuote()
          .then(({ shouldDeclineNow }) =>
            shouldDeclineNow ? fsm.declineOffer(true) : Promise.resolve()
          )
          .then(() => fsm.exitMiddleware())
          .catch(() => fsm.exitMiddleware())
      }

      if (helper.isAcceptOfferIntention(session)) {
        return fsm.acceptOffer()
          .then(() => fsm.exitMiddleware())
          .catch(() => fsm.exitMiddleware())
      }

      if (helper.isDeclineOfferIntention(session)) {
        return fsm.declineOffer()
          .then(() => fsm.exitMiddleware())
          .catch(() => fsm.exitMiddleware())
      }
    // initiator specific transitions
    } else if (session.userType === USER_TYPES.TRADER) {
      if (helper.isCancelIntention(session)) {
        return fsm.cancelTrade()
          .then(() => fsm.exitMiddleware())
          .catch(() => fsm.exitMiddleware())
      }
      if (helper.isObjectionIntention(session)) {
        return fsm.handleObjection()
          .then(() => fsm.exitMiddleware())
          .catch(() => fsm.exitMiddleware())
      }
      if (helper.isTradeOptionPresent(session)) {
        return fsm.markTradeOptionSpecified()
          .then(() => {
            if (helper.isTradeRequestIntention(session)) {
              return fsm.sendTradeRequest()
            } else {
              return fsm.noOp()
            }
          })
          .then(() => fsm.exitMiddleware())
          .catch(() => fsm.exitMiddleware())
      }
    }

    console.log('Output Message:', session.outputMessage)

    // if nothing matched until here
    fsm.exitMiddleware()
  })
    .catch(err => {
      console.log('Flow control handling failed, ERR:', err)

      fsm.exitMiddleware()
    })
}

export default new FSMMiddlware()
