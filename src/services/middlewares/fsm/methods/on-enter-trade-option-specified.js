import { TransactionsSessionChainRepo } from '../../../../repositories'

/**
 * Handles the case when the initiator has specified a trade option
 * (BUY/SELL/BID/OFFER).
 * @param  {Object} session User's session.
 */
const onEnterTradeOptionSpecified = ({ session }) => {
  if (!session.hasChainId() || session.isReceiverSession()) {
    return Promise.resolve()
  }

  return TransactionsSessionChainRepo
    .updateById(session.getChainId(), { tradeOptionSpecified: true })
}

export default onEnterTradeOptionSpecified
