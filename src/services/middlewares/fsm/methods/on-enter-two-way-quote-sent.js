import events, { eventNames } from
  '../../../../services/events-system/event-emitter'
import { TransactionRepo } from '../../../../repositories'
import { SmartManager, TransactionsSessionManager } from '../../../../managers'
import { TRANSACTION_STATUSES } from '../../../../constants'
import helper from '../fsm-helper'
import decisionControl from '../../../../decision-control'
import messages from '../../../../user-messages'

/**
 * Takes the actions necessary when a receiver specifies a two way quote
 * (bid/offer spread).
 * @param  {Object} session User's session.
 */
const onEnterTwoWayQuoteSent = ({ session }) => {
  // return if trader is not a receiver (only receivers have activeTransaction on session!)
  if (!session.activeTransaction) {
    return Promise.reject(new Error('The trader is not a receiver'))
  }

  let chain = null
  let transaction = null
  let transactionsSession = null
  let financialInstrument = null

  // find the activeTransaction's dependencies
  return SmartManager
    .findAsManyDependenciesOf(
      { transactionId: session.activeTransaction.getId() }
    )
    .then(dependencies => {
      chain = dependencies.chain
      transaction = dependencies.transaction
      transactionsSession = dependencies.transactionsSession
      financialInstrument = dependencies.financialInstrument

      try {
        // update transaction
        transaction.setBidValue(helper.getQuoteBid(session))
        transaction.setOfferValue(helper.getQuoteOffer(session))
        transaction.setOfferVolume(helper.getQuoteQuantity(session))
        transaction.setReceiverSessionId(session.id)
        transaction.setStatus(TRANSACTION_STATUSES.RECEIVER_ANSWERED)

        return transaction.save()
      } catch (err) {
        console.log('!!! onEnterTwoWayQuoteSent ERR', err)
      }
    })
    .then(transactionEntity => {
      transaction = transactionEntity

      let bidPrice = transaction.getBidValue()
      let offerPrice = transaction.getOfferValue()
      let financialInstrumentLabel = financialInstrument.getLabel()
      let shouldTellOfferIsInvalid = null

      // decide if offer is valid or invalid
      shouldTellOfferIsInvalid = decisionControl.shouldTellOfferIsInvalid({
        bidPrice,
        offerPrice,
        financialInstrument: financialInstrumentLabel
      })

      if (shouldTellOfferIsInvalid) {
        // inform the offer is invalid
        return sendOfferIsInvalid(
          transactionsSession,
          transaction,
          [
            messages.receiver.invalidOfferFn({
              spreadString: messages.spreadFn(bidPrice, offerPrice),
              financialInstrument: financialInstrumentLabel
            }),
            messages.receiver.freeToStartANewTrade
          ]
        )
      }

      // if offer is valid continue
      return Promise.resolve()
    })
    .then(() => {
      let bidPrice = transaction.getBidValue()
      let offerPrice = transaction.getOfferValue()
      let shouldDeclineAutomatically = decisionControl
        .shouldDeclineTransactionAutomatically({
          bidPrice,
          offerPrice,
          tradeOption: chain.getTradeOption(),
          priceToleranceLimit: chain.getPriceToleranceLimit()
        })

      if (
        transaction.isIcebergTrade() &&
        chain.getTradeActionWasExpressed() &&
        !shouldDeclineAutomatically
      ) {
        executeIcebergTradeTransaction(
          chain,
          transaction,
          transactionsSession,
          financialInstrument
        )
      }

      return Promise.resolve({ shouldDeclineNow: shouldDeclineAutomatically })
    })
}

const executeIcebergTradeTransaction = (
  chain,
  transaction,
  transactionsSession,
  financialInstrument
) => {
  let startExecutionNow = decisionControl
    .shouldExecuteTransactionAutomatically({
      priceToleranceLimit: chain.getPriceToleranceLimit(),
      bidPrice: transaction.getBidValue(),
      offerPrice: transaction.getOfferValue(),
      tradeOption: chain.getTradeOption()
    })

  if (startExecutionNow) {
    events.emit(eventNames.HANDLE_DIRECT_TRADE_ACTION, {
      transactionsSessionId: transactionsSession.getId(),
      tradeAction: chain.getTradeOption(),
      customTransactionId: transaction.getId()
    })
  }
}

const sendOfferIsInvalid = (transactionsSession, transaction, message) => {
  return TransactionRepo
    .updateById(
      transaction.getId(),
      { status: TRANSACTION_STATUSES.INVALID_OFFER }
    )
    .then(() => {
      TransactionsSessionManager
        .excludeTransactionIdsFromTransactionsSession(
          transactionsSession,
          [transaction.getId()]
        )

      return transactionsSession.save()
    })
    .then(result => {
      events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
        traderIds: [transaction.getReceiverId()],
        message
      })
    })
}

export default onEnterTwoWayQuoteSent
