import { TRANSACTION_STATUSES } from '../../../../constants'
import { SmartManager } from '../../../../managers'
import {
  createCompletedTransaction
} from '../../../common/completed-transaction-creator'
import events, { eventNames } from '../../../events-system/event-emitter'
import dashboardAPI from '../../../dashboard-api-wrapper'
import messages from '../../../../user-messages'

/**
 * Defines the behaviour when a receiver confirms a trade confirmation
 * question.
 */
const onEnterOfferAccepted = ({ session }) => {
  const transaction = session.activeTransaction

  if (!transaction) {
    return Promise.reject(new Error('No active transaction.'))
  }
  // if confirmation question was not asked
  if (transaction.getStatus() !== TRANSACTION_STATUSES.CONFIRMATION_ASKED) {
    return Promise.resolve()
  }

  // reset watson context
  session.setContext = true
  session.watson = {}
  session.initiatorWorkspaceId = null
  session.receiverWorkspaceId = null
  session.transactionsSessionId = null
  session.transactionsSessionChainId = null

  if (transaction.isIcebergTrade()) {
    // if iceberg transaction, handle this in its own way
    return SmartManager
      .findAsManyDependenciesOf({ transactionId: transaction.getId() })
      .then(({
        transaction,
        transactionsSession,
        chain,
        financialInstrument
      }) => {
        // if not enough trade volume left, abort and announce the trader
        if (chain.getTradeVolumeLeft() < transaction.getOfferVolume()) {
          events.emit(eventNames.SEND_MESSAGE_TO_TRADERS, {
            traderIds: [transaction.getReceiverId()],
            message: messages.receiver.tradeRefusedNoVolumeAvailable
          })

          return
        }

        // decrease the available shards number
        chain.setAvailableShards(chain.getAvailableShards() - 1)
        // save the transaction id for which the shard was sold
        chain.addSoldShardTransactionId(transaction.getId())
        // decrease the trade volume left
        chain.setTradeVolumeLeft(
          chain.getTradeVolumeLeft() - transaction.getOfferVolume()
        )

        // if no price limit was set
        if (!chain.getPriceToleranceLimit()) {
          chain.setTradeOptionSpecified(false)
        }

        // accept transaction
        transaction.setStatus(TRANSACTION_STATUSES.ACCEPTED)
        transaction.setCloseAction(chain.getTradeOption())

        // create completed transaction
        createCompletedTransaction(transaction, transactionsSession, chain)
          .then(completedTransaction => {
            // send mail
            events.emit(eventNames.SEND_TRANSACTION_CONFIRMATION_EMAIL, {
              completedTransaction
            })
            // create completed transaction
            events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
              traderIds: [transaction.getReceiverId()],
              message: messages.receiver.transactionCongratulation
            })
            // send message to initiator to inform him about the completed transaction
            sendUpdatedTradeStatusToInitiator(
              transaction,
              chain,
              financialInstrument
            )

            dashboardAPI.notifyDashboardTransactions()
          })

        return Promise.all([chain.save(), transaction.save()])
      })
  }

  // handle standard trade scenario
  return SmartManager
    .findAsManyDependenciesOf({ transactionId: transaction.getId() })
    .then(({ chain, financialInstrument }) => {
      // if sufficient trade volume left, close transaction
      if (chain.getTradeVolumeLeft() >= transaction.getOfferVolume()) {
        transaction.setStatus(TRANSACTION_STATUSES.ACCEPTED)
        transaction.setCloseAction(chain.getTradeOption())
        chain.setTradeVolumeLeft(
          chain.getTradeVolumeLeft() - transaction.getOfferVolume()
        )
        // send message to initiator to inform him about the completed transaction
        sendUpdatedTradeStatusToInitiator(
          transaction,
          chain,
          financialInstrument
        )
      } else {
        // inform receiver that the transaction could not be closed
        events.emit(eventNames.SEND_MESSAGE_TO_TRADERS, {
          traderIds: [transaction.getReceiverId()],
          message: messages.receiver.tradeRefusedNoVolumeAvailable
        })
      }

      return Promise.all([chain.save(), transaction.save()])
    })

  // send message to initiator to inform him about the completed transaction
  function sendUpdatedTradeStatusToInitiator (
    transaction,
    chain,
    financialInstrument
  ) {
    events.emit(eventNames.SEND_MESSAGE_TO_TRADERS, {
      traderIds: [chain.getInitiatorId()],
      message: messages.initiator.tradeStatusUpdateFn({
        totalVolume: chain.getTotalTradedVolume(),
        remainingVolume: chain.getTradeVolumeLeft(),
        tradedVolume: chain.getTotalTradedVolume() -
          chain.getTradeVolumeLeft(),
        financialInstrument: financialInstrument.getLabel()
      })
    })
  }
}

export default onEnterOfferAccepted
