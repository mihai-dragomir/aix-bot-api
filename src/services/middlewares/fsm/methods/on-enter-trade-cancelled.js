import { TransactionsSessionChainRepo } from '../../../../repositories'
import { SESSIONS_CHAIN_STATUSES } from '../../../../constants'

/**
 * Handles cancel from initiator. Initiator can stop the entire chain.
 * @param  {Object} session   User's session.
 */
const onEnterTradeCancelled = ({ session }) => {
  const isCancelledByInitiator = !session.transaction
  const transactionsSessionChainId = session.transactionsSessionChainId

  session.watson = {}
  session.setContext = true
  session.initiatorWorkspaceId = null
  session.receiverWorkspaceId = null
  session.transactionsSessionChainId = null

  if (!isCancelledByInitiator) { // this should not happen
    return Promise.resolve()
  }

  return TransactionsSessionChainRepo
    .findById(transactionsSessionChainId)
    .then(chain => {
      chain.setStatus(SESSIONS_CHAIN_STATUSES.CANCELLED)
      chain.setActiveTransactionsSessionId(null)

      return chain.save()
    })
}

export default onEnterTradeCancelled
