import { TRANSACTION_STATUSES, USER_TYPES } from '../../../../constants'
import messages from '../../../../user-messages'
import { TransactionsSession } from '../../../../api/transactions-session'

/**
 * Exected when a receiver sends a bid/offer and after that, it cancels it.
 * @param  {Object} session User's session.
 */
const onEnterTwoWayQuoteRefused = ({ session }) => {
  let transaction = session.activeTransaction

  // if reached here but the trader is not a receiver (only receivers have activeTransaction on session!)
  if (!session.activeTransaction) {
    return Promise.reject(new Error('Trader is not a receiver'))
  }

  // update transaction status
  transaction.status = TRANSACTION_STATUSES.CLOSED_BY_RATE_REQUEST_REFUSAL
  transaction.marketMakerSessionId = session.id

  return transaction.save()
    .then(transaction =>
      TransactionsSession.findById(transaction.getTransactionsSessionId())
    )
    .then(transactionsSession => {
      // remove this transaction from the transactions session evidence
      let index = transactionsSession.transactions.indexOf(transaction.getId())

      if (index >= 0) {
        transactionsSession.transactions.splice(index, 1)

        // confirm to the used that the quote was refused
        session.outputMessage = [messages.receiver.traderRateRequestRefused]

        session.watson = {}
        session.setContext = true
        session.userType = USER_TYPES.TRADER
        session.initiatorWorkspaceId = null
        session.receiverWorkspaceId = null

        return transactionsSession.save()
      }

      return Promise.reject(new Error('onEnterTwoWayQuoteRefused error'))
    })
}

export default onEnterTwoWayQuoteRefused
