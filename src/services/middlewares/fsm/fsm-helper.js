import numeral from 'numeral'

/**
 * A collection of helper functions that read or analyze different fields on the
 * user session. This is used as a higher order component, to
 * avoid working directly with the session.
 *
 * OBSOLETE: Please use session-wrapper instead !
 */

export const isRateRequestIntention = session => {
  return session.watson.context.intent_name === 'rate_request' &&
    session.watson.context.request_quantity
}

export const isIcebergTradeAllowed = session => {
  return session.watson.context.request_iceberg
}

export const isTradeRequestIntention = session => {
  return session.watson.context.intent_name === 'trade_request'
}

export const isTradeOptionPresent = session => {
  return ['buy', 'sell', 'bid', 'offer']
    .includes(session.watson.context.trade_action)
}

export const isBuyAction = session => {
  return session.watson.context.trade_action === 'buy'
}

export const isSellAction = session => {
  return session.watson.context.trade_action === 'sell'
}

export const isBidAction = session => {
  return session.watson.context.trade_action === 'bid'
}

export const isOfferAction = session => {
  return session.watson.context.trade_action === 'offer'
}

export const isTwoWayQuoteIntention = session => {
  return session.watson.context.intent_name === 'two_way_quote'
}

export const isAcceptOfferIntention = session => {
  return session.watson.context.intent_name === 'accept_offer'
}

export const isDeclineOfferIntention = session => {
  return session.watson.context.intent_name === 'decline_offer'
}

export const isCancelIntention = session => {
  return session.watson.context.intent_name === 'cancel'
}

export const isObjectionIntention = session => {
  return session.watson.context.intent_name === 'objection'
}

export const intentionIsMissing = watsonUpdate => {
  return !watsonUpdate.context.intent_name
}

export const isTraderNotRegistered = session => {
  return !session.trader
}

export const isTraderAReceiver = session => {
  return !!session.activeTransaction
}

export const isTraderAnInitiator = session => {
  return !session.activeTransaction
}

export const isFlowControlPoint = session => {
  return !!session.watson.context.flow_control
}

export const getFlowControlValue = session => {
  return session.watson.context.flow_control
}

export const getFinancialInstrumentLabel = session => {
  if (!session.watson || !session.watson.context) {
    return null
  }

  return session.watson.context.request_crypto_symbol
}

export const checkIfTraderHasFinancianInstrument = (
  session,
  financialInstrument
) => {
  let financialInstruments = session.financialInstruments

  if (!financialInstruments) {
    return false
  }

  return financialInstruments.some(
    instrument => instrument.label === financialInstrument
  )
}

export const getFinancialInstrument = (session, financialInstrumentLabel) => {
  let financialInstruments = session.financialInstruments

  for (let i = 0; i < financialInstruments.length; i++) {
    if (financialInstruments[i].label === financialInstrumentLabel) {
      return financialInstruments[i]
    }
  }

  return null
}

export const getRequestQuantity = session => {
  let quantity = session.watson.context.request_quantity

  if (!quantity) {
    return null
  }
  if (typeof quantity === 'string') {
    return numeral(quantity.toLowerCase()).value()
  }

  return quantity
}

export const getQuoteQuantity = session => {
  let quantity = session.watson.context.quote_quantity

  if (!quantity) {
    return null
  }
  if (typeof quantity === 'string') {
    return numeral(quantity.toLowerCase()).value()
  }

  return quantity
}

export const getRequestAction = session => {
  return session.watson.context.request_action
}

export const getRequestMonth = session => {
  return session.watson.context.request_month
}

export const getRequestYear = session => {
  return session.watson.context.request_year
}

export const getRequestValue = session => {
  return session.watson.context.request_value
}

export const getObjectionTradePhase = session => {
  return session.watson.context.objection
}

export const getObjectionObject = session => {
  return session.watson.context.objection_object
}

export const getShardSize = session => {
  return session.watson.context.request_shard_size
}

export const getTradeSize = session => {
  return session.watson &&
    session.watson.context &&
    session.watson.context.trade_size
}

export const getQuoteOffer = session => {
  return session.watson.context.quote_offer
}

export const getQuoteBid = session => {
  return session.watson.context.quote_bid
}

export const getTradeValue = session => {
  let numericString = session.watson.context.trade_value

  if (!numericString || typeof numericString !== 'string') {
    return numericString
  }

  return numeral(numericString.toLowerCase()).value()
}

export const getPriceToleranceLimit = session => {
  let assumedNumericString = session.watson.context.request_price_limit

  if (!assumedNumericString) {
    return null
  }
  if (typeof assumedNumericString === 'number') {
    return assumedNumericString
  }

  return numeral(assumedNumericString.toLowerCase()).value()
}

export const watsonErrorHandler = (err, session) => {
  if (err.output.error) {
    session.watsonErrorMessage = [err.output.error]
  } else {
    session.watsonErrorMessage = [
      'Sorry. It looks like our AI service has a problem. ' +
      'Please try again later.'
    ]
  }
}

export default {
  isRateRequestIntention,
  isTradeRequestIntention,
  intentionIsMissing,
  isTwoWayQuoteIntention,
  isIcebergTradeAllowed,
  isAcceptOfferIntention,
  isDeclineOfferIntention,
  isObjectionIntention,
  isCancelIntention,
  isTradeOptionPresent,
  isBuyAction,
  isSellAction,
  isBidAction,
  isOfferAction,
  isTraderNotRegistered,
  isTraderAReceiver,
  isTraderAnInitiator,
  isFlowControlPoint,
  getFlowControlValue,
  getFinancialInstrumentLabel,
  checkIfTraderHasFinancianInstrument,
  getFinancialInstrument,
  getRequestQuantity,
  getQuoteQuantity,
  getRequestAction,
  getRequestMonth,
  getRequestYear,
  getRequestValue,
  getObjectionTradePhase,
  getObjectionObject,
  getShardSize,
  getQuoteOffer,
  getQuoteBid,
  getTradeValue,
  getPriceToleranceLimit,
  getTradeSize,
  watsonErrorHandler
}
