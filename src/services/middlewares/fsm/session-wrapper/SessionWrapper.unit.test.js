import SessionWrapper from './SessionWrapper'
import {
  SessionNotDefinedError,
  SessionFieldOverrideError,
  SessionFieldUnassignError
} from '../../../common/errors'

let sessionWrapperInst = null

beforeEach(() => {
  sessionWrapperInst = new SessionWrapper()
})

test('Create a new instance', () => {
  expect(sessionWrapperInst).toMatchObject({})
})

test('Contains wrapperFunctions property [array]', () => {
  expect(sessionWrapperInst.hasOwnProperty('wrapperFunctions')).toBeTruthy()
  expect(Array.isArray(sessionWrapperInst.wrapperFunctions)).toBeTruthy()
})

test(
  'Registered functions from wrapperFunctions have the correct format',
  () => {
    sessionWrapperInst.wrapperFunctions.forEach(fnConfig => {
      expect(fnConfig).toMatchObject({
        name: expect.any(String),
        fn: expect.any(Function)
      })
    })
  }
)

test('Test hasChainId()', () => {
  sessionWrapperInst.transactionsSessionChainId = undefined
  expect(sessionWrapperInst.hasChainId()).toBeFalsy()

  sessionWrapperInst.transactionsSessionChainId = 0
  expect(sessionWrapperInst.hasChainId()).toBeFalsy()

  sessionWrapperInst.transactionsSessionChainId = 1
  expect(sessionWrapperInst.hasChainId()).toBeFalsy()

  sessionWrapperInst.transactionsSessionChainId = Infinity
  expect(sessionWrapperInst.hasChainId()).toBeFalsy()

  sessionWrapperInst.transactionsSessionChainId = -Infinity
  expect(sessionWrapperInst.hasChainId()).toBeFalsy()

  sessionWrapperInst.transactionsSessionChainId = -20
  expect(sessionWrapperInst.hasChainId()).toBeFalsy()

  sessionWrapperInst.transactionsSessionChainId = false
  expect(sessionWrapperInst.hasChainId()).toBeFalsy()

  sessionWrapperInst.transactionsSessionChainId = true
  expect(sessionWrapperInst.hasChainId()).toBeFalsy()

  sessionWrapperInst.transactionsSessionChainId = '1'
  expect(sessionWrapperInst.hasChainId()).toBeTruthy()

  sessionWrapperInst.transactionsSessionChainId = '1234567890'
  expect(sessionWrapperInst.hasChainId()).toBeTruthy()
})

test('Test isReceiverSession()', () => {
  sessionWrapperInst.activeTransaction = undefined
  expect(sessionWrapperInst.isReceiverSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = true
  expect(sessionWrapperInst.isReceiverSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = false
  expect(sessionWrapperInst.isReceiverSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = 0
  expect(sessionWrapperInst.isReceiverSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = -100
  expect(sessionWrapperInst.isReceiverSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = Infinity
  expect(sessionWrapperInst.isReceiverSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = null
  expect(sessionWrapperInst.isReceiverSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = {}
  expect(sessionWrapperInst.isReceiverSession()).toBeTruthy()
})

test('Test isInitiatorSession()', () => {
  sessionWrapperInst.activeTransaction = undefined
  expect(sessionWrapperInst.isInitiatorSession()).toBeTruthy()

  sessionWrapperInst.activeTransaction = null
  expect(sessionWrapperInst.isInitiatorSession()).toBeTruthy()

  sessionWrapperInst.activeTransaction = true
  expect(sessionWrapperInst.isInitiatorSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = false
  expect(sessionWrapperInst.isInitiatorSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = 0
  expect(sessionWrapperInst.isInitiatorSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = -100
  expect(sessionWrapperInst.isInitiatorSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = Infinity
  expect(sessionWrapperInst.isInitiatorSession()).toBeFalsy()

  sessionWrapperInst.activeTransaction = {}
  expect(sessionWrapperInst.isInitiatorSession()).toBeFalsy()
})

test('Test getChainId()', () => {
  sessionWrapperInst.transactionsSessionChainId = '123'
  expect(sessionWrapperInst.getChainId()).toBe('123')

  sessionWrapperInst.transactionsSessionChainId = undefined
  expect(sessionWrapperInst.getChainId()).toBe(undefined)
})

test('Test hasWatsonContext()', () => {
  sessionWrapperInst.watson = null
  expect(sessionWrapperInst.hasWatsonContext()).toBeFalsy()

  sessionWrapperInst.watson = undefined
  expect(sessionWrapperInst.hasWatsonContext()).toBeFalsy()

  sessionWrapperInst.watson = Infinity
  expect(sessionWrapperInst.hasWatsonContext()).toBeFalsy()

  sessionWrapperInst.watson = {}
  expect(sessionWrapperInst.hasWatsonContext()).toBeFalsy()

  sessionWrapperInst.watson = { test: 'test' }
  expect(sessionWrapperInst.hasWatsonContext()).toBeFalsy()

  sessionWrapperInst.watson = { context: null }
  expect(sessionWrapperInst.hasWatsonContext()).toBeFalsy()

  sessionWrapperInst.watson = { context: true }
  expect(sessionWrapperInst.hasWatsonContext()).toBeFalsy()

  sessionWrapperInst.watson = { context: Infinity }
  expect(sessionWrapperInst.hasWatsonContext()).toBeFalsy()

  sessionWrapperInst.watson = { context: {} }
  expect(sessionWrapperInst.hasWatsonContext()).toBeTruthy()
})

test('Test wrap() function', () => {
  expect(() => sessionWrapperInst.wrap(null))
    .toThrowError(SessionNotDefinedError)
  expect(() => sessionWrapperInst.wrap())
    .toThrowError(SessionNotDefinedError)

  expect(() => sessionWrapperInst.wrap({})).not.toThrow()

  expect(() => sessionWrapperInst.wrap({ hasChainId: () => {} }))
    .toThrowError(SessionFieldOverrideError('hasChainId'))
  expect(() => sessionWrapperInst.wrap({ hasChainId: true }))
    .toThrowError(SessionFieldOverrideError('hasChainId'))
})

test('Test unwrap() function', () => {
  expect(() => sessionWrapperInst.unwrap({ hasChainId: true }))
    .toThrowError(SessionFieldUnassignError('hasChainId', 'function'))

  let session = { hasChainId: () => {} }

  expect(() => sessionWrapperInst.unwrap(session))
    .not.toThrowError(SessionFieldUnassignError('hasChainId', 'function'))

  expect(session.hasOwnProperty('hasChainId')).toBeFalsy()
})
