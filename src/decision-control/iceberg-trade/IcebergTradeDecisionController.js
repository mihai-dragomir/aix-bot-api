import DecisionController from '../DecisionController'
import marketPrices from '../../services/market-prices'

class IcebergTradeDecisionController extends DecisionController {
  constructor () {
    super()

    this.AVERAGE_VOLUME_MULTIPLIER = 5

    this.shouldAskForIcebergTrade = this.shouldAskForIcebergTrade.bind(this)
    this.getMarketAverageTradeVolume =
      this.getMarketAverageTradeVolume.bind(this)
    this.shouldFinalizeIcebergTrade =
      this.shouldFinalizeIcebergTrade.bind(this)
  }

  // decides between normal and iceberg trade flow
  shouldAskForIcebergTrade (requestQuantity) {
    if (typeof requestQuantity !== 'number') {
      return false
    }
    let marketAverageTradeVolume = this.getMarketAverageTradeVolume()

    if (typeof marketAverageTradeVolume !== 'number') {
      return false
    }

    return requestQuantity >= marketAverageTradeVolume *
      this.AVERAGE_VOLUME_MULTIPLIER
  };

  getMarketAverageTradeVolume () {
    return marketPrices.getMarketAverageTradeVolume()
  }

  shouldFinalizeIcebergTrade (params) {
    const {
      isIcebergTrade,
      chainStatus,
      duration,
      maxDuration,
      entireChainTradeRequestAnswersNo,
      entireChainPossibleOffersNo,

      tradeVolumeLeft,
      shardVolume
    } = params

    let remainingTime = maxDuration - duration
    let recheckPeriod = this.TRANSACTIONS_SESSION_STATUS_RECHECK_PERIOD

    if (!isIcebergTrade) {
      return false
    }

    if (chainStatus === this.SESSIONS_CHAIN_STATUSES.FINISHED) {
      return false
    }

    if (tradeVolumeLeft === 0 || tradeVolumeLeft < shardVolume) {
      return true
    }

    return (
      typeof entireChainTradeRequestAnswersNo === 'number' &&
      typeof entireChainPossibleOffersNo === 'number' &&
      entireChainTradeRequestAnswersNo === entireChainPossibleOffersNo
    ) || remainingTime <= 2 * recheckPeriod
  };
}

export default IcebergTradeDecisionController
