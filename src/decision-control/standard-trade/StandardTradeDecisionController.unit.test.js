import StandardTradeDecisionController from './StandardTradeDecisionController'
import { SESSIONS_CHAIN_STATUSES } from '../../constants'

describe('StandardTradeDecisionController', () => {
  let controllerInst = null
  let testedFn = null

  beforeEach(() => {
    controllerInst = new StandardTradeDecisionController()
  })

  describe('instance', () => {
    test('should contain the needed constants', () => {
      expect(controllerInst).toMatchObject({
        SESSIONS_CHAIN_STATUSES: expect.any(Object)
      })
    })
  })

  describe('standardTradeDecisionController() function', () => {
    beforeEach(() => { testedFn = controllerInst.shouldFinalizeStandardTrade })

    test('should return correct decision when iceberg trade is on', () => {
      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        expect(testedFn(getParams(true, status, 1))).toBeFalsy()
        expect(testedFn(getParams(true, status, 1, 1))).toBeFalsy()
        expect(testedFn(getParams(true, status, 1, 0))).toBeFalsy()
        expect(testedFn(getParams('true', status, 1))).toBeFalsy()
        expect(testedFn(getParams(true, status, 0))).toBeFalsy()
        expect(testedFn(getParams(true, status, 'test'))).toBeFalsy()
        expect(testedFn(getParams(true, status, null))).toBeFalsy()
        expect(testedFn(getParams(true, status, null))).toBeFalsy()
      })
    })

    test('should return correct decision when iceberg trade is off', () => {
      Object.keys(SESSIONS_CHAIN_STATUSES).forEach(status => {
        if (status === SESSIONS_CHAIN_STATUSES.FINISHED) {
          expect(testedFn(getParams(false, status, 1))).toBeFalsy()
          expect(testedFn(getParams(false, status, 0))).toBeFalsy()
          expect(testedFn(getParams(false, status, 'test'))).toBeFalsy()
          expect(testedFn(getParams(false, status, null))).toBeFalsy()
        } else {
          expect(testedFn(getParams(false, status, 1, 0))).toBeTruthy()
          expect(testedFn(getParams(false, status, 1, 2))).toBeFalsy()
          expect(testedFn(getParams(null, status, 1))).toBeFalsy()
          expect(testedFn(getParams(undefined, status, 1))).toBeFalsy()
          expect(testedFn(getParams('false', status, 1))).toBeFalsy()
          expect(testedFn(getParams(false, status, 'test'))).toBeFalsy()
          expect(testedFn(getParams(false, status, 0))).toBeFalsy()
          expect(testedFn(getParams(false, status, null))).toBeFalsy()
        }
      })
    })

    function getParams (
      isIcebergTrade,
      chainStatus,
      entireChainTradeRequestAnswersNo,
      tradeVolumeLeft
    ) {
      return {
        isIcebergTrade,
        chainStatus,
        entireChainTradeRequestAnswersNo,
        tradeVolumeLeft
      }
    }
  })
})
