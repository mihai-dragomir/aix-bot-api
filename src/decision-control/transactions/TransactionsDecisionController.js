import DecisionController from '../DecisionController'

class TransactionsDecisionController extends DecisionController {
  constructor () {
    super()

    this.shouldExecuteTransactionAutomatically =
      this.shouldExecuteTransactionAutomatically.bind(this)
    this.shouldDeclineTransactionAutomatically =
      this.shouldDeclineTransactionAutomatically.bind(this)
  }

  shouldExecuteTransactionAutomatically (params) {
    if (!this.USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG) {
      return false
    }
    const {
      bidPrice,
      offerPrice,
      tradeOption,
      priceToleranceLimit
    } = params

    let priceToleranceLimitExists = !!priceToleranceLimit &&
      typeof priceToleranceLimit === 'number'
    let offerIsValid = typeof offerPrice === 'number' && offerPrice >= 0
    let bidIsValid = typeof bidPrice === 'number' && bidPrice >= 0

    if (!priceToleranceLimitExists || !(offerIsValid || bidIsValid)) {
      return false
    }

    switch (tradeOption) {
      case this.TRADE_ACTIONS.BUY:
      case this.TRADE_ACTIONS.BID:
        return offerIsValid && offerPrice <= priceToleranceLimit
      case this.TRADE_ACTIONS.SELL:
      case this.TRADE_ACTIONS.OFFER:
        return bidIsValid && bidPrice >= priceToleranceLimit
      default: return false
    }
  }

  shouldDeclineTransactionAutomatically (params) {
    if (!this.USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG) {
      return false
    }
    const {
      bidPrice,
      offerPrice,
      tradeOption,
      priceToleranceLimit
    } = params

    let priceToleranceLimitExists = !!priceToleranceLimit &&
      typeof priceToleranceLimit === 'number'
    let offerIsValid = typeof offerPrice === 'number' && offerPrice >= 0
    let bidIsValid = typeof bidPrice === 'number' && bidPrice >= 0

    if (!priceToleranceLimitExists || !(offerIsValid || bidIsValid)) {
      return false
    }

    switch (tradeOption) {
      case this.TRADE_ACTIONS.BUY:
      case this.TRADE_ACTIONS.BID:
        return offerIsValid && offerPrice > priceToleranceLimit
      case this.TRADE_ACTIONS.SELL:
      case this.TRADE_ACTIONS.OFFER:
        return bidIsValid && bidPrice < priceToleranceLimit
      default: return false
    }
  }
};

export default TransactionsDecisionController
