import TransactionsDecisionController from './TransactionsDecisionController'

describe('TransactionsDecisionController', () => {
  let controllerInst = null
  let testedFn = null
  let buyParams = null
  let bidParams = null
  let sellParams = null
  let offerParams = null

  beforeEach(() => {
    controllerInst = new TransactionsDecisionController()
    buyParams = {
      bidPrice: 100,
      offerPrice: 90,
      tradeOption: controllerInst.TRADE_ACTIONS.BUY,
      priceToleranceLimit: 120
    }
    bidParams = {...buyParams, tradeOption: controllerInst.TRADE_ACTIONS.BID}
    sellParams = {
      bidPrice: 130,
      offerPrice: 90,
      tradeOption: controllerInst.TRADE_ACTIONS.SELL,
      priceToleranceLimit: 120
    }
    offerParams = {
      ...sellParams,
      tradeOption: controllerInst.TRADE_ACTIONS.OFFER
    }
  })

  describe('instance', () => {
    test('should contain the needed constants', () => {
      expect(controllerInst).toMatchObject({
        USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG: expect.any(Boolean),
        TRADE_ACTIONS: expect.any(Object)
      })
    })
  })

  describe('shouldExecuteTransactionAutomatically function', () => {
    describe(
      'with USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG = true',
      () => {
        beforeEach(() => {
          controllerInst.USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG = true
          testedFn = controllerInst.shouldExecuteTransactionAutomatically
        })

        test('auto buy decision should be correct', () => {
          expect(testedFn({...buyParams})).toBeTruthy()
          expect(testedFn({ ...buyParams, bidPrice: 130 })).toBeTruthy()
          expect(testedFn({ ...buyParams, bidPrice: null })).toBeTruthy()
          expect(testedFn({ ...buyParams, bidPrice: undefined })).toBeTruthy()
          expect(testedFn({ ...buyParams, offerPrice: 0 })).toBeTruthy()
          expect(testedFn({ ...buyParams, offerPrice: 120 })).toBeTruthy()
          expect(testedFn({ ...buyParams, offerPrice: -90 })).toBeFalsy()
          expect(testedFn({ ...buyParams, offerPrice: null })).toBeFalsy()
          expect(testedFn({ ...buyParams, offerPrice: {} })).toBeFalsy()
          expect(testedFn({ ...buyParams, offerPrice: 1000 })).toBeFalsy()
          expect(testedFn({ ...buyParams, priceToleranceLimit: null }))
            .toBeFalsy()
          expect(testedFn({ ...buyParams, priceToleranceLimit: {} }))
            .toBeFalsy()
        })

        test('auto bid decision should be correct', () => {
          expect(testedFn({...bidParams})).toBeTruthy()
          expect(testedFn({ ...bidParams, bidPrice: 130 })).toBeTruthy()
          expect(testedFn({ ...bidParams, bidPrice: null })).toBeTruthy()
          expect(testedFn({ ...bidParams, bidPrice: undefined })).toBeTruthy()
          expect(testedFn({ ...bidParams, offerPrice: 0 })).toBeTruthy()
          expect(testedFn({ ...bidParams, offerPrice: 120 })).toBeTruthy()
          expect(testedFn({ ...bidParams, offerPrice: -90 })).toBeFalsy()
          expect(testedFn({ ...bidParams, offerPrice: null })).toBeFalsy()
          expect(testedFn({ ...bidParams, offerPrice: {} })).toBeFalsy()
          expect(testedFn({ ...bidParams, offerPrice: 1000 })).toBeFalsy()
          expect(testedFn({ ...bidParams, priceToleranceLimit: null }))
            .toBeFalsy()
          expect(testedFn({ ...bidParams, priceToleranceLimit: {} }))
            .toBeFalsy()
        })

        test('auto sell decision should be correct', () => {
          expect(testedFn({...sellParams})).toBeTruthy()
          expect(testedFn({ ...sellParams, bidPrice: -90 })).toBeFalsy()
          expect(testedFn({ ...sellParams, bidPrice: 0 })).toBeFalsy()
          expect(testedFn({ ...sellParams, bidPrice: 120 })).toBeTruthy()
          expect(testedFn({ ...sellParams, bidPrice: 1000 })).toBeTruthy()
          expect(testedFn({ ...sellParams, bidPrice: null })).toBeFalsy()
          expect(testedFn({ ...sellParams, offerPrice: null })).toBeTruthy()
          expect(testedFn({ ...sellParams, offerPrice: 100 })).toBeTruthy()
          expect(testedFn({ ...sellParams, offerPrice: null })).toBeTruthy()
          expect(testedFn({ ...sellParams, offerPrice: {} })).toBeTruthy()
          expect(testedFn({ ...sellParams, priceToleranceLimit: null }))
            .toBeFalsy()
          expect(testedFn({ ...sellParams, priceToleranceLimit: {} }))
            .toBeFalsy()
        })

        test('auto offer decision should be correct', () => {
          expect(testedFn({ ...offerParams })).toBeTruthy()
          expect(testedFn({ ...offerParams, bidPrice: -90 })).toBeFalsy()
          expect(testedFn({ ...offerParams, bidPrice: 0 })).toBeFalsy()
          expect(testedFn({ ...offerParams, bidPrice: 120 })).toBeTruthy()
          expect(testedFn({ ...offerParams, bidPrice: 1000 })).toBeTruthy()
          expect(testedFn({ ...offerParams, bidPrice: null })).toBeFalsy()
          expect(testedFn({ ...offerParams, offerPrice: null })).toBeTruthy()
          expect(testedFn({ ...offerParams, offerPrice: 100 })).toBeTruthy()
          expect(testedFn({ ...offerParams, offerPrice: null })).toBeTruthy()
          expect(testedFn({ ...offerParams, offerPrice: {} })).toBeTruthy()
          expect(testedFn({ ...offerParams, priceToleranceLimit: null }))
            .toBeFalsy()
          expect(testedFn({ ...offerParams, priceToleranceLimit: {} }))
            .toBeFalsy()
        })
      }
    )

    describe(
      'with USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG = false',
      () => {
        beforeEach(() => {
          controllerInst.USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG = false
          testedFn = controllerInst.shouldExecuteTransactionAutomatically
        })

        test('returns false in any case', () => {
          expect(testedFn()).toBeFalsy()
          expect(testedFn({})).toBeFalsy()
        })
      }
    )
  })

  describe('shouldDeclineTransactionAutomatically function', () => {
    describe(
      'with USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG = true',
      () => {
        beforeEach(() => {
          controllerInst.USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG = true
          testedFn = controllerInst.shouldDeclineTransactionAutomatically
        })

        test('decline buy decision should be correct', () => {
          expect(testedFn({...buyParams})).toBeFalsy()
          expect(testedFn({ ...buyParams, bidPrice: 130 })).toBeFalsy()
          expect(testedFn({ ...buyParams, bidPrice: null })).toBeFalsy()
          expect(testedFn({ ...buyParams, bidPrice: undefined })).toBeFalsy()
          expect(testedFn({ ...buyParams, offerPrice: 0 })).toBeFalsy()
          expect(testedFn({ ...buyParams, offerPrice: 120 })).toBeFalsy()
          expect(testedFn({ ...buyParams, offerPrice: 1000 })).toBeTruthy()
          expect(testedFn({ ...buyParams, offerPrice: -90 })).toBeFalsy()
          expect(testedFn({ ...buyParams, offerPrice: null })).toBeFalsy()
          expect(testedFn({ ...buyParams, offerPrice: {} })).toBeFalsy()
          expect(testedFn({ ...buyParams, priceToleranceLimit: 1000 }))
            .toBeFalsy()
          expect(testedFn({ ...buyParams, priceToleranceLimit: null }))
            .toBeFalsy()
          expect(testedFn({ ...buyParams, priceToleranceLimit: {} }))
            .toBeFalsy()
        })

        test('decline bid decision should be correct', () => {
          expect(testedFn({...bidParams})).toBeFalsy()
          expect(testedFn({ ...bidParams, bidPrice: 130 })).toBeFalsy()
          expect(testedFn({ ...bidParams, bidPrice: null })).toBeFalsy()
          expect(testedFn({ ...bidParams, bidPrice: undefined })).toBeFalsy()
          expect(testedFn({ ...bidParams, offerPrice: 0 })).toBeFalsy()
          expect(testedFn({ ...bidParams, offerPrice: 120 })).toBeFalsy()
          expect(testedFn({ ...bidParams, offerPrice: 1000 })).toBeTruthy()
          expect(testedFn({ ...bidParams, offerPrice: -90 })).toBeFalsy()
          expect(testedFn({ ...bidParams, offerPrice: null })).toBeFalsy()
          expect(testedFn({ ...bidParams, offerPrice: {} })).toBeFalsy()
          expect(testedFn({ ...bidParams, offerPrice: 1000 })).toBeTruthy()
          expect(testedFn({ ...bidParams, priceToleranceLimit: 1000 }))
            .toBeFalsy()
          expect(testedFn({ ...bidParams, priceToleranceLimit: null }))
            .toBeFalsy()
          expect(testedFn({ ...bidParams, priceToleranceLimit: {} }))
            .toBeFalsy()
        })

        test('decline sell decision should be correct', () => {
          expect(testedFn({...sellParams})).toBeFalsy()
          expect(testedFn({ ...sellParams, bidPrice: -90 })).toBeFalsy()
          expect(testedFn({ ...sellParams, bidPrice: 0 })).toBeTruthy()
          expect(testedFn({ ...sellParams, bidPrice: 120 })).toBeFalsy()
          expect(testedFn({ ...sellParams, bidPrice: 1000 })).toBeFalsy()
          expect(testedFn({ ...sellParams, bidPrice: null })).toBeFalsy()
          expect(testedFn({ ...sellParams, bidPrice: 100, offerPrice: null }))
            .toBeTruthy()
          expect(testedFn({ ...sellParams, bidPrice: 100, offerPrice: {} }))
            .toBeTruthy()
          expect(testedFn({ ...sellParams, bidPrice: 100, offerPrice: 1000 }))
            .toBeTruthy()
          expect(testedFn({ ...sellParams, priceToleranceLimit: 1000 }))
            .toBeTruthy()
          expect(testedFn({ ...sellParams, priceToleranceLimit: null }))
            .toBeFalsy()
          expect(testedFn({ ...sellParams, priceToleranceLimit: {} }))
            .toBeFalsy()
        })

        test('decline offer decision should be correct', () => {
          expect(testedFn({ ...offerParams })).toBeFalsy()
          expect(testedFn({ ...offerParams, bidPrice: -90 })).toBeFalsy()
          expect(testedFn({ ...offerParams, bidPrice: 0 })).toBeTruthy()
          expect(testedFn({ ...offerParams, bidPrice: 120 })).toBeFalsy()
          expect(testedFn({ ...offerParams, bidPrice: 1000 })).toBeFalsy()
          expect(testedFn({ ...offerParams, bidPrice: null })).toBeFalsy()
          expect(testedFn({ ...offerParams, bidPrice: 100, offerPrice: null }))
            .toBeTruthy()
          expect(testedFn({ ...offerParams, bidPrice: 100, offerPrice: 100 }))
            .toBeTruthy()
          expect(testedFn({ ...offerParams, bidPrice: 100, offerPrice: null }))
            .toBeTruthy()
          expect(testedFn({ ...offerParams, bidPrice: 100, offerPrice: {} }))
            .toBeTruthy()
          expect(testedFn({ ...offerParams, priceToleranceLimit: null }))
            .toBeFalsy()
          expect(testedFn({ ...offerParams, priceToleranceLimit: {} }))
            .toBeFalsy()
        })
      }
    )

    describe(
      'with USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG = false',
      () => {
        beforeEach(() => {
          controllerInst.USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG = false
          testedFn = controllerInst.shouldDeclineTransactionAutomatically
        })

        test('returns false in any case', () => {
          expect(testedFn()).toBeFalsy()
          expect(testedFn({})).toBeFalsy()
        })
      }
    )
  })
})
