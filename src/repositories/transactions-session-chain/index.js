import Transaction from '../transaction'
import TransactionsSession from '../transactions-session'
import FinancialInstrumentRepo from '../financial-instrument'
import { TransactionsSessionChain } from '../../api/transactions-session-chain'

const findById = (chainId, projection = null) => {
  return TransactionsSessionChain.findById(chainId)
}

const findFinancialInstrumentOfChain = (chain, projection = null) => {
  return FinancialInstrumentRepo.findById(chain.getFinancialInstrumentId())
}

const findTransactionsSessionsOfChainWithId = (chainId, projection) => {
  return TransactionsSession.find(
    { transactionsSessionChainId: chainId },
    projection
  )
}

const findChainReceiverTransactions = (chainId, filter, projection = null) => {
  return findById(chainId)
    .then(chain =>
      TransactionsSession.findByIds(chain.getTransactionsSessionIds())
    )
    .then(sessions => {
      let ids = sessions.map(session => session.getTransactionIds())
      let allTransactionIds = [].concat(...ids)

      if (!filter) {
        return Transaction.findByIds(allTransactionIds, projection)
      }
      if (filter.fromNotClosedTransactions) {
        return Transaction.findNotClosedByIds(allTransactionIds, projection)
      }
      if (filter.fromNotClosedAndNotDecidedTransactions) {
        console.log('allTransactionIds', allTransactionIds)

        return Transaction
          .findNotClosedAndNotDecidedByIds(allTransactionIds, projection)
      }
      if (filter.fromNotActiveTransactions) {
        return Transaction.findNotActiveByIds(allTransactionIds, projection)
      }
      if (filter.fromCancelledTransactions) {
        return Transaction.findCancelledByIds(allTransactionIds, projection)
      }
      if (filter.fromOpenTransactions) {
        return Transaction.findOpenByIds(allTransactionIds, projection)
      }
      console.log('ERR in findChainReceiverTraderIds: Filter specified but ' +
        'no filter properties matched.')

      return []
    })
}

const findChainReceiverTransactionIds = (chainId, filter) => {
  return findChainReceiverTransactions(
    chainId,
    filter,
    { _id: 1 }
  )
    .then(transactionEntities => {
      let receiverTransactionIds = transactionEntities.map(t => t.getId())

      return Promise.resolve(receiverTransactionIds)
    })
}

const findChainReceiverTraderIds = (chainId, filter) => {
  return findChainReceiverTransactions(
    chainId,
    filter,
    { marketMakerId: 1 }
  )
    .then(transactionEntities => {
      let receiverTraderIds = transactionEntities.map(t => t.getReceiverId())

      return Promise.resolve(receiverTraderIds)
    })
}

const updateAllTransactionStatusExceptTheOneWithId = (
  chainId, notWantedId, newStatus
) => {
  return findById(chainId)
    .then(chain => TransactionsSession
      .findByIds(chain.getTransactionsSessionIds())
    )
    .then(sessions => {
      let ids = sessions.map(TransactionsSession.getTransactionIds)
      let transactionIds = [].concat(...ids).filter(id => id !== notWantedId)

      return Transaction.updateByIds(transactionIds, { status: newStatus })
    })
}

const findChainOfTransactionsSession = (
  transactionsSession,
  projection = null
) => {
  return findById(transactionsSession.getChainId())
}

const updateById = (id, updatedFields = null) => {
  return TransactionsSessionChain.update(
    { _id: id },
    updatedFields
  ).exec()
}

export default {
  findById,
  findFinancialInstrumentOfChain,
  findTransactionsSessionsOfChainWithId,

  findChainReceiverTraderIds,
  findChainReceiverTransactions,
  findChainReceiverTransactionIds,
  findChainOfTransactionsSession,

  updateAllTransactionStatusExceptTheOneWithId,
  updateById
}
