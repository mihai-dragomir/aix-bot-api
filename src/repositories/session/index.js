import { Types } from 'mongoose'

import { Session } from '../../api/session'

const deleteAllForTraderWithId = traderId => {
  return Session
    .remove({ 'session.trader._id': Types.ObjectId(traderId) })
}

const updateAllForTraderWithId = (traderId, updatedFields, callback) => {
  Session
    .update(
      { 'session.trader._id': Types.ObjectId(traderId) },
      { $set: updatedFields },
      { multi: true },
      (err, result) => {
        if (err) {
          return callback(err)
        }
        if (result.nModified > 0) {
          return callback(null, traderId)
        }

        console.log('Overwrite trader ' + traderId + ' session result:', result)
        return callback()
      })
}

const deleteAllWithNoSessionData = () => {
  return Session
    .remove({ 'session.watson': { $exists: false } })
}

export default {
  deleteAllForTraderWithId,
  updateAllForTraderWithId,
  deleteAllWithNoSessionData
}
