/*
  In order to provide a valid api config, firstly you must copy the content
  of each config sample file to a file with the same name but without the
  .sample suffix in it. Ex: (watson.sample.js => watson.js). Secondly you must
  fill the private/sensitive information in each .js config file you created.
  Ex: (username & password for email service, db uri)
*/

/* eslint-disable no-unused-vars */
import _ from 'lodash'
import path from 'path'

/* istanbul ignore next */
const requireProcessEnv = (name) => {
  if (!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable')
  }
  return process.env[name]
}

/* istanbul ignore next */
if (process.env.NODE_ENV === 'local') {
  const dotenv = require('dotenv-safe')
  dotenv.load({
    path: path.join(__dirname, '../../.env'),
    sample: path.join(__dirname, '../../.env.example')
  })
}

const databaseConfig = _.merge({},
  getDatabaseConfigEnvironment(),
  getConfigFile('database')
)

const mailConfig = _.merge({},
  getEmailConfigEnvironment(),
  getConfigFile('mail')
)

const watsonConfig = _.merge({},
  getWatsonConfigEnvironment(),
  getConfigFile('watson')
)

const s3Config = _.merge({},
  getS3ConfigEnvironment(),
  getConfigFile('aws-s3')
)

const config = {
  all: {
    env: process.env.NODE_ENV || 'local',
    root: path.join(__dirname, '../..'),
    port: process.env.PORT || 9000,
    ip: process.env.IP || '0.0.0.0',
    masterKey: requireProcessEnv('MASTER_KEY'),
    jwtSecret: requireProcessEnv('JWT_SECRET'),
    apiRoot: process.env.API_ROOT || '/api',
    botmasterApiRoot: process.env.BOTMASTER_API || 'http://0.0.0.0:3000/trader',
    cryptoCompareApiRoot: 'https://min-api.cryptocompare.com/data',
    cardsApiRoot: process.env.CARDS_API || 'http://0.0.0.0:9000/api',
    dashboardApiRoot: process.env.DASHBOARD_API || 'http://0.0.0.0:5000/api',
    mongoSessionsCollection: 'sessions',
    mongo: databaseConfig.ALL_ENV,
    watsonConfig: watsonConfig,
    sendgridemail: mailConfig,
    s3: s3Config,
    serviceDesk: getServiceDeskConfigEnvironment()
  },
  local: {
    mongo: databaseConfig.local
  },
  development: {
    mongo: databaseConfig.DEVELOPMENT
  },
  test: {
    mongo: databaseConfig.TEST
  },
  unit_test: {
    mongo: databaseConfig.UNIT_TEST
  },
  staging: {
    mongo: databaseConfig.STAGING
  },
  production: {
    ip: process.env.IP || undefined,
    port: process.env.PORT || 8080,
    mongo: databaseConfig.PRODUCTION
  }
}

function getConfigFile (configFilename) {
  var fileName = './' + configFilename
  var config

  try {
    config = require(fileName)
  } catch (err) {
    config = {}
  }

  return config.default !== undefined ? config.default : {}
}

function getDatabaseConfigEnvironment () {
  return {
    ALL_ENV: {
      options: {
        db: {
          safe: true
        }
      }
    },
    DEVELOPMENT: {
      uri: process.env.MONGODB_URL,
      options: {
        debug: true
      }
    },
    TEST: {
      uri: process.env.MONGODB_URL,
      options: {
        debug: false
      }
    },
    UNIT_TEST: {
      uri: process.env.MONGODB_URL,
      options: {
        debug: false
      }
    },
    STAGING: {
      uri: process.env.MONGODB_URL,
      options: {
        debug: false
      }
    },
    PRODUCTION: {
      uri: process.env.MONGODB_URL,
      options: {
      }
    },
    local: {
      uri: '',
      options: {
        debug: true
      }
    }
  }
}

function getEmailConfigEnvironment () {
  return {
    apikey: process.env.MAIL_API_KEY,
    from: process.env.MAIL_ADRESS_FROM
  }
}

function getWatsonConfigEnvironment () {
  return {
    username: process.env.WATSON_USERNAME,
    password: process.env.WATSON_PASSWORD,
    url: process.env.WATSON_API,
    crypto: {
      initiatorWorkspaceId: process.env.WATSON_WS_CRYPTO_INITIATOR,
      receiverWorkspaceId: process.env.WATSON_WS_CRYPTO_RECEIVER
    },
    futureCommodity: {
      initiatorWorkspaceId: process.env.WATSON_WS_FUTURE_COMMODITY_INITIATOR,
      receiverWorkspaceId: process.env.WATSON_WS_FUTURE_COMMODITY_RECEIVER
    }
  }
}

function getS3ConfigEnvironment () {
  return {
    accessKeyId: 'AKIAI6OPRKVGQIJP22QQ',
    secretAccessKey: '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',
    bucket: 's3-saltandpepper'
  }
}

function getServiceDeskConfigEnvironment () {
  return {
    serviceDeskMainUrl: process.env.SERVICE_DESK_MAIN_URL,
    serviceDeskBasicAuth: process.env.SERVICE_DESK_BASIC_AUTH_TOKEN,
    projectId: process.env.SERVICE_DESK_PROJECT_ID,
    issueTypeId: process.env.SERVICE_DESK_ISSUE_TYPE_ID,
    assigne: process.env.SERVICE_DESK_ASSIGNE
  }
}

module.exports = Object.assign(config.all, config[config.all.env])
export default module.exports
