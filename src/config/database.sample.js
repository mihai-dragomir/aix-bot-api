export default {
  ALL_ENV: {
    options: {
      db: {
        safe: true
      }
    }
  },
  DEVELOPMENT: {
    uri: process.env.MONGODB_URL,
    options: {
      debug: true
    }
  },
  TEST: {
    uri: process.env.MONGODB_URL,
    options: {
      debug: false
    }
  },
  UNIT_TEST: {
    uri: process.env.MONGODB_URL,
    options: {
      debug: false
    }
  },
  STAGING: {
    uri: process.env.MONGODB_URL,
    options: {
      debug: false
    }
  },
  PRODUCTION: {
    uri: process.env.MONGODB_URL,
    options: {
    }
  },
  local: {
    uri: '', // Update this with your local MongDb Uri
    options: {
      debug: true
    }
  }
}
