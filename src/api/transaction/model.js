import mongoose, { Schema } from 'mongoose'

const transactionSchema = new Schema({
  marketMakerSessionId: {
    type: String
  },
  transactionsSessionId: {
    type: String
  },
  traderId: {
    type: String
  },
  marketMakerId: {
    type: String
  },
  watsonContext: {
    type: Object
  },
  bidVolume: {
    type: Number
  },
  offerVolume: {
    type: Number
  },
  bidValue: {
    type: Number
  },
  offerValue: {
    type: Number
  },
  status: {
    type: String
  },
  closeAction: {
    type: String
  },
  icebergTrade: {
    type: Boolean
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

transactionSchema.methods = {
  getId () {
    return this.id
  },
  getOfferValue () {
    return this.offerValue
  },
  getBidValue () {
    return this.bidValue
  },
  getOfferVolume () {
    return this.offerVolume
  },
  getReceiverSessionId () {
    return this.marketMakerSessionId
  },
  getTransactionsSessionId () {
    return this.transactionsSessionId
  },
  getStatus () {
    return this.status
  },
  getCloseAction () {
    return this.closeAction
  },
  getInitiatorId () {
    return this.traderId
  },
  getReceiverId () {
    return this.marketMakerId
  },

  isIcebergTrade () {
    return this.icebergTrade
  },

  setOfferValue (value) {
    this.offerValue = value
  },
  setBidValue (value) {
    this.bidValue = value
  },
  setOfferVolume (volume) {
    this.offerVolume = volume
  },
  setReceiverSessionId (sessionId) {
    this.marketMakerSessionId = sessionId
  },
  setTransactionsSessionId (id) {
    this.transactionsSessionId = id
  },
  setStatus (newStatus) {
    this.status = newStatus
  },
  setCloseAction (newValue) {
    this.closeAction = newValue
  },

  view (full) {
    const view = {
      // simple view
      id: this.id,
      marketMakerSessionId: this.marketMakerSessionId,
      transactionsSessionId: this.transactionsSessionId,
      traderId: this.traderId,
      marketMakerId: this.marketMakerId,
      watsonContext: this.watsonContext,
      status: this.status,
      bidVolume: this.bidVolume,
      offerVolume: this.offerVolume,
      bidValue: this.bidValue,
      offerValue: this.offerValue,
      closeAction: this.closeAction,
      icebergTrade: this.icebergTrade,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Transaction', transactionSchema)

export const schema = model.schema
export default model
