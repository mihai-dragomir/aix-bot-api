import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { BotMessage } from '.'
import { signSync } from '../../services/jwt'
import { Trader } from '../trader'

const app = () => express(apiRoot, routes)

let botMessage, trader, traderSession

beforeEach(async () => {
  botMessage = await BotMessage.create({})

  trader = await Trader.create({ telegramId: '123456789', password: '123456' })
  traderSession = signSync(trader.id)
})

test('POST /bot-messages 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ message: 'test', senderId: 'test' })
  expect(status).toBe(401)
})

test('POST /bot-messages 200', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ message: 'test', senderId: 'test' })
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.message).toEqual('test')
  expect(body.senderId).toEqual('test')
})

test('GET /bot-messages 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /bot-messages 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /bot-messages/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${botMessage.id}`)
  expect(status).toBe(401)
})

test('GET /bot-messages/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${botMessage.id}`)
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(botMessage.id)
})

test('GET /bot-messages/:id 401', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(401)
})

test('GET /bot-messages/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(404)
})

test('PUT /bot-messages/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${botMessage.id}`)
    .send({ message: 'test', senderId: 'test' })
  expect(status).toBe(401)
})

test('PUT /bot-messages/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${botMessage.id}`)
    .send({ message: 'test', senderId: 'test' })
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(botMessage.id)
  expect(body.message).toEqual('test')
  expect(body.senderId).toEqual('test')
})

test('PUT /bot-messages/:id 401', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ message: 'test', senderId: 'test' })
  expect(status).toBe(401)
})

test('PUT /bot-messages/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ message: 'test', senderId: 'test' })
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(404)
})

test('DELETE /bot-messages/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${botMessage.id}`)
  expect(status).toBe(401)
})

test('DELETE /bot-messages/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${botMessage.id}`)
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(204)
})

test('DELETE /bot-messages/:id 401', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(401)
})

test('DELETE /bot-messages/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(404)
})
