import { success, notFound } from '../../services/response/'
import { BotMessage } from '.'

export const create = ({ bodymen: { body }, session }, res, next) => {
  body.sessionId = session.id

  if (process.env.NODE_ENV === 'unit_test') {
    return BotMessage.create(body)
      .then((botMessage) => botMessage.view(true))
      .then(success(res))
      .catch(next)
  }

  return BotMessage.create(body)
    .then((botMessage) => botMessage.view(true))
  // .then(success(res))
    .then(() => next())
    .catch(next)
}

export const index = ({ querymen: { query, select, cursor } }, res, next) => {
  return BotMessage.find(query, select, cursor)
    .then((botMessages) => {
      return botMessages.map((botMessage) => botMessage.view())
    })
    .then(success(res))
    .catch(next)
}

export const show = ({ params }, res, next) => {
  BotMessage.findById(params.id)
    .then(notFound(res))
    .then((botMessage) => botMessage ? botMessage.view() : null)
    .then(success(res))
    .catch(next)
}

export const update = ({ bodymen: { body }, params }, res, next) =>
  BotMessage.findById(params.id)
    .then(notFound(res))
    .then((botMessage) => botMessage ? Object.assign(botMessage, body).save() : null)
    .then((botMessage) => botMessage ? botMessage.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  BotMessage.findById(params.id)
    .then(notFound(res))
    .then((botMessage) => botMessage ? botMessage.remove() : null)
    .then(success(res, 204))
    .catch(next)
