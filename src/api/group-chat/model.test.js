import { GroupChat } from '.'

let groupChat

beforeEach(async () => {
  groupChat = await GroupChat.create({ message: 'test', group_name: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = groupChat.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(groupChat.id)
    expect(view.message).toBe(groupChat.message)
    expect(view.group_name).toBe(groupChat.group_name)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = groupChat.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(groupChat.id)
    expect(view.message).toBe(groupChat.message)
    expect(view.group_name).toBe(groupChat.group_name)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
