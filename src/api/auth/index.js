import { Router } from 'express'
import { login } from './controller'
import { password, master } from '../../services/passport'

const router = new Router()

/**
 * @api {post} /auth Authenticate
 * @apiName Authenticate
 * @apiGroup Auth
 * @apiPermission master
 * @apiHeader {String} Authorization Basic authorization with telegranId and password (eg: Basic NTQ5MDI4MTU5OjEyMzQ1Ng==).
 *  The header form must be: __"Basic ${credentials}"__.
 *  Credetials form must be: __convertToBase64("${telegramId}:${password}")__.
 * @apiParam {String} access_token Master access_token.
 * @apiSuccess (Success 201) {String} token User `access_token` to be passed to other requests.
 * @apiSuccess (Success 201) {Object} user Current user's data.
 * @apiError 401 Master access only or invalid credentials.
 */
router.post('/',
  master(),
  password(),
  login)

export default router
