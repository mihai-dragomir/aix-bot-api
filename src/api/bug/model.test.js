import { Bug } from '.'

let bug

beforeEach(async () => {
  bug = await Bug.create({ bug_description: 'test', conversation_id: 'test', user_telegram_id: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = bug.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(bug.id)
    expect(view.bug_description).toBe(bug.bug_description)
    expect(view.conversation_id).toBe(bug.conversation_id)
    expect(view.user_telegram_id).toBe(bug.user_telegram_id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = bug.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(bug.id)
    expect(view.bug_description).toBe(bug.bug_description)
    expect(view.conversation_id).toBe(bug.conversation_id)
    expect(view.user_telegram_id).toBe(bug.user_telegram_id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
