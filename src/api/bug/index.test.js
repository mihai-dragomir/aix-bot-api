import request from 'supertest'
import { apiRoot, masterKey } from '../../config'
import express from '../../services/express'
import routes from '.'
import Bug from './model'
import { Trader } from '../trader'
import { signSync } from '../../services/jwt'

const app = () => express(apiRoot, routes)

let bug, trader, traderSession

beforeEach(async () => {
  bug = await Bug.create({})
  trader = await Trader.create({ telegramId: '123456789', password: '123456' })
  traderSession = signSync(trader.id)
})

test('POST /bugs 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ bug_description: 'test', conversation_id: 'test', user_telegram_id: 'test' })
  expect(status).toBe(401)
})

test('POST /bugs 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ bug_description: 'test', conversation_id: 'test', user_telegram_id: 'test' })
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.bug_description).toEqual('test')
  expect(body.conversation_id).toEqual('test')
  expect(body.user_telegram_id).toEqual('test')
})

test('GET /bugs 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /bugs 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /bugs/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${bug.id}`)
  expect(status).toBe(401)
})

test('GET /bugs/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${bug.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(bug.id)
})

test('GET /bugs/:id 401', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(401)
})

test('GET /bugs/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})

test('PUT /bugs/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${bug.id}`)
    .send({ bug_description: 'test', conversation_id: 'test', user_telegram_id: 'test' })
  expect(status).toBe(401)
})

test('PUT /bugs/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${bug.id}`)
    .send({ access_token: masterKey, bug_description: 'test', conversation_id: 'test', user_telegram_id: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(bug.id)
  expect(body.bug_description).toEqual('test')
  expect(body.conversation_id).toEqual('test')
  expect(body.user_telegram_id).toEqual('test')
})

test('PUT /bugs/:id 401', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ bug_description: 'test', conversation_id: 'test', user_telegram_id: 'test' })
  expect(status).toBe(401)
})

test('PUT /bugs/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, bug_description: 'test', conversation_id: 'test', user_telegram_id: 'test' })
  expect(status).toBe(404)
})

test('DELETE /bugs/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${bug.id}`)
  expect(status).toBe(401)
})

test('DELETE /bugs/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${bug.id}`)
    .send({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /bugs/:id 401', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(401)
})

test('DELETE /bugs/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey })
  expect(status).toBe(404)
})
