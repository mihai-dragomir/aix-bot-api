import bcrypt from 'bcrypt'
import { env } from '../../config'
import mongoose, { Schema } from 'mongoose'

const walletSchema = new Schema({
  currency: String,
  address: String
})

const bankDetailsSchena = new Schema({
  beneficiaryName: String,
  beneficiaryAddress: String,
  accountNumber: String,
  routingNumber: String,
  bankName: String,
  bankAddress: String
})

const traderSchema = new Schema({
  telegramId: {
    type: String,
    match: /^[0-9]{9}$/,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 6
  },
  mobileNumber: {
    type: String
  },
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  email: {
    type: String
  },
  companyName: {
    type: String
  },
  sessionId: {
    type: String
  },
  financialInstrumentsInUse: {
    type: Array
  },
  defaultPaymentMethod: {
    type: Boolean
  },
  traderType: {
    type: String
  },
  bankDetails: {
    type: bankDetailsSchena
  },
  wallet: {
    type: [walletSchema]
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

traderSchema.pre('save', function (next) {
  if (!this.isModified('password')) return next()

  /* istanbul ignore next */
  const rounds = env === 'test' ? 1 : 9

  bcrypt.hash(this.password, rounds).then((hash) => {
    this.password = hash
    next()
  }).catch(next)
})

traderSchema.methods = {
  getId () {
    return this.id
  },
  getTelegramId () {
    return this.telegramId
  },
  getFirstName () {
    return this.firstName
  },
  getLastName () {
    return this.lastName
  },
  getEmail () {
    return this.email
  },
  getWallet () {
    return this.wallet
  },
  getBankDetails () {
    return this.bankDetails
  },
  view (full) {
    const view = {
      // simple view
      id: this.id,
      telegramId: this.telegramId,
      mobileNumber: this.mobileNumber,
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      bankDetails: this.bankDetails,
      wallet: this.wallet,
      companyName: this.companyName,
      financialInstrumentsInUse: this.financialInstrumentsInUse,
      defaultPaymentMethod: this.defaultPaymentMethod,
      traderType: this.traderType,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  },

  authenticate (password) {
    return bcrypt.compare(password, this.password).then((valid) => valid ? this : false)
  }
}

const model = mongoose.model('Trader', traderSchema)

export const schema = model.schema
export default model
