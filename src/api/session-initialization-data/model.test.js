import { SessionInitializationData } from '.'

let sessionInitializationData

beforeEach(async () => {
  sessionInitializationData = await SessionInitializationData.create({ traderTelegramId: 'test', initiallizationData: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = sessionInitializationData.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(sessionInitializationData.id)
    expect(view.traderTelegramId).toBe(sessionInitializationData.traderTelegramId)
    expect(view.initiallizationData).toBe(sessionInitializationData.initiallizationData)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = sessionInitializationData.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(sessionInitializationData.id)
    expect(view.traderTelegramId).toBe(sessionInitializationData.traderTelegramId)
    expect(view.initiallizationData).toBe(sessionInitializationData.initiallizationData)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
