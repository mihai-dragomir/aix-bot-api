import mongoose, { Schema } from 'mongoose'

const sessionInitializationDataSchema = new Schema({
  traderTelegramId: {
    type: String
  },
  initiallizationData: {
    type: Object
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

sessionInitializationDataSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      traderTelegramId: this.traderTelegramId,
      initiallizationData: this.initiallizationData,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('SessionInitializationData', sessionInitializationDataSchema)

export const schema = model.schema
export default model
