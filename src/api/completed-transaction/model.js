import mongoose, { Schema } from 'mongoose'

const completedTransactionSchema = new Schema({
  tradedFinancialInstrumentId: {
    type: String
  },
  initiatorTraderId: {
    type: String
  },
  marketMakerTraderId: {
    type: String
  },
  transactionsSessionId: {
    type: String
  },
  transactionId: {
    type: String
  },
  dealVolume: {
    type: String
  },
  dealPrice: {
    type: String
  },
  dealMonth: {
    type: String
  },
  dealYear: {
    type: String
  },
  dealAction: {
    type: String
  },
  dealValue: {
    type: String
  },
  dealShards: {
    type: String
  },
  closeAction: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

completedTransactionSchema.methods = {
  getId () {
    return this.id
  },
  getInitiatorId () {
    return this.initiatorTraderId
  },
  getReceiverId () {
    return this.marketMakerTraderId
  },
  getTransactionsSessionId () {
    return this.transactionsSessionId
  },
  getTransactionId () {
    return this.transactionId
  },
  getFinancialInstrumentId () {
    return this.tradedFinancialInstrumentId
  },
  getPrice () {
    return this.dealPrice
  },
  getVolume () {
    return this.dealVolume
  },
  getCloseAction () {
    return this.closeAction
  },
  getMonth () {
    return this.dealMonth
  },
  getYear () {
    return this.dealYear
  },
  getValue () {
    return this.dealValue
  },
  getDealAction () {
    return this.dealAction
  },
  getCreatedAt () {
    return this.createdAt
  },

  view (full) {
    const view = {
      // simple view
      id: this.id,
      tradedFinancialInstrumentId: this.tradedFinancialInstrumentId,
      initiatorTraderId: this.initiatorTraderId,
      marketMakerTraderId: this.marketMakerTraderId,
      transactionsSessionId: this.transactionsSessionId,
      transactionId: this.transactionId,
      dealVolume: this.dealVolume,
      dealPrice: this.dealPrice,
      dealMonth: this.dealMonth,
      dealYear: this.dealYear,
      dealAction: this.dealAction,
      dealShards: this.dealShards,
      dealValue: this.dealValue,
      closeAction: this.closeAction,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('CompletedTransaction', completedTransactionSchema)

export const schema = model.schema
export default model
